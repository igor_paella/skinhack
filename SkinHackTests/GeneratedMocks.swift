// MARK: - Mocks generated from file: SkinHack/BL/Facade/TestFacadeProtocol.swift at 2017-05-16 10:49:34 +0000

//
//  TestFacadeProtocol.swift
//  SkinHack
//
//  Created by Игорь on 16/05/2017.
//  Copyright © 2017 xuli. All rights reserved
import Cuckoo
@testable import SkinHack

import Foundation

class MockTestFacadeProtocol: TestFacadeProtocol, Cuckoo.Mock {
    typealias MocksType = TestFacadeProtocol
    typealias Stubbing = __StubbingProxy_TestFacadeProtocol
    typealias Verification = __VerificationProxy_TestFacadeProtocol
    let cuckoo_manager = Cuckoo.MockManager()

    private var observed: TestFacadeProtocol?

    func spy(on victim: TestFacadeProtocol) -> Self {
        observed = victim
        return self
    }

    

    

    
     func test()  {
        
        return cuckoo_manager.call("test()",
            parameters: (),
            original: observed.map { o in
                return { () in
                    o.test()
                }
            })
        
    }
    

    struct __StubbingProxy_TestFacadeProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager

        init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        func test() -> Cuckoo.StubNoReturnFunction<()> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub("test()", parameterMatchers: matchers))
        }
        
    }


    struct __VerificationProxy_TestFacadeProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation

        init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }

        

        
        @discardableResult
        func test() -> Cuckoo.__DoNotUse<Void> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify("test()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
    }


}

 class TestFacadeProtocolStub: TestFacadeProtocol {
    

    

    
     func test()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
}



