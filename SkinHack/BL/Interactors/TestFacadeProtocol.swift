//
//  TestFacadeProtocol.swift
//  SkinHack
//
//  Created by Игорь on 16/05/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol TestFacadeProtocol {
    func test()
}
