//
//  FirebaseRepository.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

class FirebaseRepository {
    static let defaultStore = { () -> Firestore in
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        let db = Firestore.firestore()
        db.settings = settings
        return db
    }()
    
    var store: Firestore {
        return FirebaseRepository.defaultStore
    }
    
    func add(collection: String, values: [String: Any], callback: ((Bool) -> Void)?) {
        store.collection(collection).addDocument(data: values) { error in
            callback?(error == nil)
        }
    }
    
    func update(collection: String, doc: String, values: [String: Any], merge: Bool, callback: ((Bool) -> Void)?) {
        let document = store
            .collection(collection)
            .document(doc)
        if merge {
            document.setData(values, options: SetOptions.merge()) { error in
                callback?(error == nil)
            }
        } else {
            document.setData(values) { error in
                callback?(error == nil)
            }
        }
        
    }
    
    func add(collection: String, doc: String, subcollection: String, values: [String: Any], callback: ((Bool) -> Void)?) {
        store
            .collection(collection)
            .document(doc)
            .collection(subcollection)
            .document()
            .setData(values) { error in
                callback?(error == nil)
        }
    }
    
    func get<T: Codable & JSON>(_: T.Type, collection: String, doc: String, callback: @escaping (T?) -> Void) {
        store.collection(collection).document(doc).getDocument { (snapshot, _) in
            if snapshot?.exists == true,
                let dict = snapshot?.data(),
                let model = decode(T.self, json: dict) {
                callback(model)
            } else {
                callback(nil)
            }
        }
    }
    
    func getAll<T: Codable & JSON>(_: T.Type, collection: String, process: ((Query) -> Query)? = nil, callback: @escaping ([T]) -> Void) {
        var query: Query = store.collection(collection)
        if let process = process {
            query = process(query)
        }
        query.getDocuments { (snapshot, _) in
            if let snapshot = snapshot {
                //                if !snapshot.metadata.isFromCache {
                var result: [T] = []
                for doc in snapshot.documents {
                    if let model = decode(T.self, json: doc.data()) {
                        result.append(model)
                    }
                }
                callback(result)
                //                }
            }
        }
    }
    
    func delete(path: String, process: ((Query) -> Query)? = nil, callback: @escaping (Bool) -> Void) {
        var query: Query = store.collection(path)
        if let process = process {
            query = process(query)
        }
        query.getDocuments { [weak self] (snapshot, _) in
            guard let strongSelf = self else { return }
            if let snapshot = snapshot {
                let collection = strongSelf.store.collection(path)
                let batch = collection.firestore.batch()
                snapshot.documents.forEach({ batch.deleteDocument($0.reference) })
                batch.commit { error in
                    callback(error == nil)
                }
            } else {
                callback(true)
            }
        }
    }
    
    func deleteDocument(path: String, document: String, callback: @escaping (Bool) -> Void) {
        store.collection(path).document(document).delete { error in
            callback(error == nil)
        }
    }
    
    func deleteField(path: String, document: String, field: String, callback: @escaping (Bool) -> Void) {
        store.collection(path).document(document).updateData([field: FieldValue.delete()]) { error in
            callback(error == nil)
        }
    }
}
