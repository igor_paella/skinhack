//
//  TShirtFacade.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import MetaWear

protocol TShirtFacadeProtocol {
    var currentDevice: MBLMetaWear? { get }

    func checkConnected(completed: @escaping (Bool) -> Void)
    func findDevices(completed: @escaping ([DeviceName]) -> Void) 
    func connect(deviceById: UUID, callback: @escaping (Bool) -> Void)
    func disconnect(completed: (() -> Void)?)
    func flash()
    func stopFlashing()
    
    func setup(personCoeff: Double)
    func startReadSensors(changed: @escaping (Double) -> Void)
    func stopReadSensors()
//    var sensorsChanged: ((Double) -> Void)? { get set }
//    var temperatireChanged: ((Double) -> Void)? { get set }
}

struct DeviceName {
    let name: String
    let id: UUID
}

let sensorInterval: Double = 5

class TShirtFacade: TShirtFacadeProtocol {
    
    var devices: [MBLMetaWear] = []
    var currentDevice: MBLMetaWear?
    var sensorsChanged: ((Double) -> Void)?
    var temperatireChanged: ((Double) -> Void)?

    private var personCoeff: Double = 1.0
    private var kkalCoeff: Double!
    private var sweatTimer: Timer?
    private var kkal: Double = 0.0
    private var bufferSweat = [Double](repeating: 0.0, count: 14)
    private var sensorsIsReading = false
    private var temperatureIsReading = false
    
    init() {
        kkalCoeff = 75*10/sensorInterval
    }
    
    func setup(personCoeff: Double) {
        self.personCoeff = personCoeff
    }
    
    func checkConnected(completed: @escaping (Bool) -> Void) {
        MBLMetaWearManager.shared().retrieveSavedMetaWearsAsync().`continue`({ [weak self] (task) -> Any? in
            if task.isCompleted {
                DispatchQueue.main.async {
                    if let device = task.result?.firstObject as? MBLMetaWear {
                        print(device.name)
                        self?.currentDevice = device
                        device.connectAsync().success { _ in
                            completed(true)
                        }
                    } else {
                        completed(false)
                    }
                }
            } else {
                completed(false)
            }
            return nil
        })
    }

    func findDevices(completed: @escaping ([DeviceName]) -> Void) {
        let timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false) { timer in
            timer.invalidate()
            completed([])
        }
        MBLMetaWearManager.shared().startScan(forMetaWearsAllowDuplicates: true, handler: { [weak self] array in
            timer.invalidate()
            self?.devices = array
            completed(array.map({ DeviceName(name: $0.name, id: $0.identifier )}))
        })
    }
    
    func connect(deviceById: UUID, callback: @escaping (Bool) -> Void) {
        if let device = devices.first(where: { $0.identifier == deviceById}) {
            MBLMetaWearManager.shared().stopScan()
            device.rememberDevice()
            device.connectAsync().success { [weak self] _ in
                self?.currentDevice = device
                callback(true)
            }.failure { _ in
                callback(false)
            }
        } else {
            callback(false)
        }
    }
    
    func disconnect(completed: (() -> Void)?) {
        currentDevice?.disconnectAsync().continueOnDispatch { _ in
            DispatchQueue.main.async {
                UserDefaults.standard.removeObject(forKey: "com.mbientlab.metawear.rememberedDevices")
                completed?()
            }
            return nil
        }
    }
    
    func flash() {
        currentDevice?.led?.flashColorAsync(UIColor.green, withIntensity: 1.0)
    }
    
    func stopFlashing() {
        currentDevice?.led?.setLEDOnAsync(false, withOptions: 1)
    }
    
    func startReadSensors(changed: @escaping (Double) -> Void) {
        kkal = 0.0
        sensorsIsReading = true
        sensorsChanged = changed
        bufferSweat = [Double](repeating: 0.0, count: 14)
        readSersorsStep()
        delay(2) {
            self.sweatTimer = Timer.scheduledTimer(timeInterval: sensorInterval, target: self, selector: #selector(TShirtFacade.sweatTimerRaised), userInfo: nil, repeats: true)
        }
    }
    private func readSersorsStep() {
        guard let currentDevice = currentDevice else { return }
        for i in 0...13 {
            currentDevice.conductance?.channels[i].readAsync().success { [weak self] data in
                guard let strongSelf = self else { return }
                strongSelf.bufferSweat[i] = data.value.doubleValue/1000
            }
        }
        
        delay(sensorInterval) { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.sensorsIsReading {
                strongSelf.readSersorsStep()
            }
        }
    }
    @objc private func sweatTimerRaised() {
        kkal += bufferSweat.average*personCoeff/kkalCoeff
        //kkal += 7
        sensorsChanged?(kkal)
    }
    
    func stopReadSensors() {
        sweatTimer?.invalidate()
        sweatTimer = nil
        sensorsIsReading = false
        sensorsChanged = nil
    }
    
    func startReadTemperature(changed: @escaping (Double) -> Void) {
        temperatureIsReading = true
        temperatireChanged = changed
        readTemperatureStep()
    }
    private func readTemperatureStep() {
        guard let temperature = currentDevice?.temperature else { return }
        temperature.onboardThermistor?.readAsync().success { data in
            DispatchQueue.main.async { [weak self] in
                self?.temperatireChanged?(data.value.doubleValue)
            }
        }
        
        delay(10) { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.temperatureIsReading {
                strongSelf.readTemperatureStep()
            }
        }
    }
    func stopReadTemperature() {
        temperatureIsReading = false
        temperatireChanged = nil
    }
}

extension Array where Element == Double {
    /// Returns the average of all elements in the array
    var average: Double {
        return isEmpty ? 0 : Double(reduce(0, +)) / Double(count)
    }
}
