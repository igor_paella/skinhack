//
//  AuthFacade.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import FirebaseAuth
import FBSDKLoginKit

class LoggedUser {
    var email: String
    var firstName: String
    var lastName: String
    var avatar: String
    var token: String
    
    init(email: String, firstName: String, lastName: String, avatar: String, token: String) {
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.avatar = avatar
        self.token = token
    }
    
    func toUser() -> User {
        let user = User(id: token, name: "\(firstName) \(lastName)", avatar: avatar, weight: nil, height: nil)
        return user
    }
}

protocol AuthFacadeProtocol {
    func auth(vc: UIViewController, callback: @escaping (LoggedUser?) -> Void)
    func logout()
    
    var isAuthorized: Bool { get }
}

class AuthFacade: AuthFacadeProtocol {
    private let loginManager = FBSDKLoginManager()
    
    func auth(vc: UIViewController, callback: @escaping (LoggedUser?) -> Void) {
        loginManager.logIn(withReadPermissions: [], from: vc) { (result, _) in
            if result?.isCancelled != true,
                let token = FBSDKAccessToken.current().tokenString {
                let credential = FacebookAuthProvider.credential(withAccessToken: token)
                Auth.auth().signIn(with: credential) { [weak self] _, _ in
                    if FBSDKAccessToken.current().tokenString != nil {
                        self?.loadFbUser(callback: callback)
                    } else {
                        callback(nil)
                    }
                }
            } else {
                callback(nil)
            }
        }
    }
    
    private func loadFbUser(callback: @escaping (LoggedUser?) -> Void) {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,first_name,last_name,email"]).start(completionHandler: { (_, result, error) -> Void in
            if error == nil,
                let dict = result as? [String: AnyObject] {
                var avatar = ""
                if let id = dict["id"] {
                    avatar = "https://graph.facebook.com/\(id)/picture?type=large"
                }
                let token = FBSDKAccessToken.current().tokenString ?? ""
                let email = (dict["email"] as? String) ?? ""
                let firstName = (dict["first_name"] as? String) ?? ""
                let lastName = (dict["last_name"] as? String) ?? ""
                let loggedUser = LoggedUser(email: email, firstName: firstName, lastName: lastName, avatar: avatar, token: token)
                callback(loggedUser)
            } else {
               callback(nil)
            }
        })
    }
    
    var isAuthorized: Bool {
        return Auth.auth().currentUser?.uid != nil
    }
    
    func logout() {
        _ = try? Auth.auth().signOut()
        FBSDKLoginManager().logOut()
    }
}
