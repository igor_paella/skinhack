//
//  TestFacade.swift
//  SkinHack
//
//  Created by Игорь on 19/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class TestFacade: TestFacadeProtocol {
    func test() {
        print("test")
    }
}
