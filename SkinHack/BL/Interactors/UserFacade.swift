//
//  UserFacade.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol UserFacadeProtocol {
    var userId: String? { get }
    
    func update(user: User, callback: @escaping (Bool) -> Void)
    func getUser(callback: @escaping (User?) -> Void)
    
    func addWorkout(kkal: Double, callback: @escaping (Bool) -> Void)
    func addWorkout(item: LeaderboardItem, callback: @escaping (Bool) -> Void)
    func getMyWorkouts(callback: @escaping ([LeaderboardItem]) -> Void)
    func getLeaderboards(callback: @escaping ([LeaderboardItem]) -> Void)
}

enum Collection: String {
    case users
    case leaderboards
}

class UserFacade: UserFacadeProtocol {
    private let repository = FirebaseRepository()
    private var user: User?

    var userId: String? {
        return Auth.auth().currentUser?.uid
    }
    
    func update(user: User, callback: @escaping (Bool) -> Void) {
        if let userId = userId,
            var json = user.json() {
            json["id"] = userId
            repository.update(collection: Collection.users.rawValue, doc: userId, values: json, merge: false) { [weak self] success in
                if success {
                    self?.user = user
                }
                callback(success)
            }
        } else {
            callback(false)
        }
    }
    
    func getUser(callback: @escaping (User?) -> Void) {
        if let userId = userId {
            if let user = user {
                callback(user)
            } else {
                repository.get(User.self, collection: Collection.users.rawValue, doc: userId) { [weak self] user in
                    if let user = user {
                        user.id = userId
                        self?.user = user
                    }
                    callback(user)
                }
            }
        } else {
            callback(nil)
        }
    }
    
    func addWorkout(item: LeaderboardItem, callback: @escaping (Bool) -> Void) {
        if let json = item.json() {
            repository.add(collection: Collection.leaderboards.rawValue, values: json, callback: callback)
        } else {
            callback(false)
        }
    }
    
    func addWorkout(kkal: Double, callback: @escaping (Bool) -> Void) {
        getUser { [weak self] user in
            if let user = user {
                let item = LeaderboardItem(id: UUID().uuidString, userId: user.id, name: user.name, avatar: user.avatar, res: Product(calories: Float(kkal)), result: kkal, date: Date())
                if let json = item.json() {
                    self?.repository.add(collection: Collection.leaderboards.rawValue, values: json, callback: callback)
                } else {
                    callback(false)
                }
            } else {
                callback(false)
            }
        }
    }
    
    func getMyWorkouts(callback: @escaping ([LeaderboardItem]) -> Void) {
        if let userId = userId {
            repository.getAll(LeaderboardItem.self, collection: Collection.leaderboards.rawValue, process: { query in
                return query.whereField("userId", isEqualTo: userId)
            }, callback: callback)
        } else {
            callback([])
        }
    }
    
    func getLeaderboards(callback: @escaping ([LeaderboardItem]) -> Void) {
        repository.getAll(LeaderboardItem.self, collection: Collection.leaderboards.rawValue, process: { query in
            return query.whereField("mmdd", isEqualTo: Date().mmdd).order(by: "result", descending: true)
        }, callback: { res in
            callback(res)
        })
    }
}
