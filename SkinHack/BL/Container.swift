//
//  Container.swift
//  SkinHack
//
//  Created by Игорь on 19/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

// swiftlint:disable force_try
// swiftlint:disable opening_brace

import Foundation
import Dip

let container = DependencyContainer { container in
    unowned let container = container
    container.register(.singleton) { TShirtFacade() as TShirtFacadeProtocol }
    container.register { AuthFacade() as AuthFacadeProtocol }
    container.register { UserFacade() as UserFacadeProtocol }
    container.register { TestFacade() as TestFacadeProtocol }
    container.register { string in TestCollectionViewModel(string: string) as TestCollectionViewModelProtocol }
    container.register {
        {
            let vm = TestViewModel()
            vm.facade = try! container.resolve() as TestFacadeProtocol
            return vm
        }() as TestViewModelProtocol
    }
    container.register {
        {
            let vm = RootViewModel()
            vm.authFacade = try! container.resolve() as AuthFacadeProtocol
            return vm
        }() as RootViewModelProtocol
    }
    container.register {
        {
            let vm = DeviceListViewModel()
            vm.tshirtFacade = try! container.resolve() as TShirtFacadeProtocol
            return vm
        }() as DeviceListViewModelProtocol
    }
    container.register {
        {
            let vm = CalendarViewModel()
            vm.tshirtFacade = try! container.resolve() as TShirtFacadeProtocol
            vm.userFacade = try! container.resolve() as UserFacadeProtocol
            return vm
        }() as CalendarViewModelProtocol
    }
    container.register {
        {
            let vm = StartViewModel()
            vm.tshirtFacade = try! container.resolve() as TShirtFacadeProtocol
            vm.userFacade = try! container.resolve() as UserFacadeProtocol
            return vm
        }() as StartViewModelProtocol
    }
    container.register { (done: ((Router) -> Void)?) in
        {
            let vm = AuthViewModel(done: done)
            vm.authFacade = try! container.resolve() as AuthFacadeProtocol
            vm.userFacade = try! container.resolve() as UserFacadeProtocol
            return vm
        }() as AuthViewModelProtocol
    }
    container.register { (done: ((Router) -> Void)?) in
        {
            let vm = WeightViewModel(done: done)
            vm.userFacade = try! container.resolve() as UserFacadeProtocol
            return vm
        }() as WeightViewModelProtocol
    }
    container.register { (done: ((Router) -> Void)?) in
        {
            let vm = HeightViewModel(done: done)
            vm.userFacade = try! container.resolve() as UserFacadeProtocol
            return vm
        }() as HeightViewModelProtocol
    }
    container.register { (isModal: Bool) in
        {
            let vm = LeaderboardViewModel(isModal: isModal)
            vm.userFacade = try! container.resolve() as UserFacadeProtocol
            return vm
        }() as LeaderboardViewModelProtocol
    }
}
