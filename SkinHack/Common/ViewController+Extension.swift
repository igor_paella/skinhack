//
//  ViewController+Extension.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

// swiftlint:disable force_cast

import Foundation

extension UIApplication {
    class var topVC: UIViewController {
        return UIApplication.shared.keyWindow!.rootViewController!.topMostViewController()
    }
}

extension UIView {
    func snapshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
}

extension UIWindow {
    func replaceRootVCWith(_ replacementController: UIViewController, animated: Bool, completion: (() -> Void)?) {
//        if rootViewController?.view.window != nil { // vc on screen
//            rootViewController?.present(replacementController, animated: animated, completion: completion)
//        } else {
            let snapshotImageView = UIImageView(image: self.snapshot())
            self.addSubview(snapshotImageView)
            rootViewController?.dismiss(animated: true, completion: {
                self.rootViewController = replacementController
                self.bringSubview(toFront: snapshotImageView)
                if animated {
                    UIView.animate(withDuration: 0.4, animations: {
                        snapshotImageView.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
                    }, completion: { _ -> Void in
                        snapshotImageView.removeFromSuperview()
                        completion?()
                    })
                } else {
                    snapshotImageView.removeFromSuperview()
                    completion?()
                }
            })
//        }
    }
}

extension UIViewController {
    
    @objc func topMostViewController() -> UIViewController {
        // Handling Modal views
        if let presentedViewController = self.presentedViewController {
            return presentedViewController.topMostViewController()
        }
            // Handling UIViewController's added as subviews to some other views.
        else {
            for view in self.view.subviews {
                // Key property which most of us are unaware of / rarely use.
                if let subViewController = view.next {
                    if subViewController is UIViewController {
                        let viewController = subViewController as! UIViewController
                        return viewController.topMostViewController()
                    }
                }
            }
            return self
        }
    }
}

extension UITabBarController {
    override func topMostViewController() -> UIViewController {
        return self.selectedViewController!.topMostViewController()
    }
}

extension UINavigationController {
    override func topMostViewController() -> UIViewController {
        return self.visibleViewController!.topMostViewController()
    }
    
    // http://stackoverflow.com/questions/19108513/uistatusbarstyle-preferredstatusbarstyle-does-not-work-on-ios-7
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        if let rootViewController = self.viewControllers.last {
            return rootViewController.preferredStatusBarStyle
        }
        return super.preferredStatusBarStyle
    }
    
}
