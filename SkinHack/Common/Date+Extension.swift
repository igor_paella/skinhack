//
//  Date+Extension.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

extension Date {
    func isEqualDay(_ date: Date) -> Bool {
        return (Calendar.current as NSCalendar).compare(self, to: date,
                                                        toUnitGranularity: .day) == ComparisonResult.orderedSame
    }
    
    var mmdd: String {
        return self.stringWithFormat("MM-dd")
    }
    
    var dayString: String {
        return self.stringWithFormat("yyyy-MM-dd")
    }
    
    func stringWithFormat(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "en-US")
        return formatter.string(from: self)
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date? {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)
    }
}
