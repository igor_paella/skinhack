//
//  Activity.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class Activity {
    class var instance: Activity {
        struct Singleton {
            static let instance = Activity()
        }
        return Singleton.instance
    }
    
    private var view: UIView!
    private var activityIndicator: UIActivityIndicatorView!
    private var activityCenterY: NSLayoutConstraint!
    
    init() {
        view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
        
        activityCenterY = activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityCenterY
        ])
    }
    
    func busy(_ busy: Bool, inView: UIView? = nil, yOffset: CGFloat = 0, backAlpha: CGFloat = 0.2) {
        if busy {
            if let rootView = inView ?? UIApplication.shared.keyWindow {
                rootView.addSubview(view)
                NSLayoutConstraint.activate([
                    view.topAnchor.constraint(equalTo: rootView.topAnchor),
                    view.bottomAnchor.constraint(equalTo: rootView.bottomAnchor),
                    view.leftAnchor.constraint(equalTo: rootView.leftAnchor),
                    view.rightAnchor.constraint(equalTo: rootView.rightAnchor)
                ])
                activityCenterY.constant = yOffset
                view.backgroundColor = UIColor.black.withAlphaComponent(backAlpha)
                activityIndicator.startAnimating()
                view.alpha = 0
                UIView.animate(withDuration: 0.5, animations: {
                    self.view.alpha = 1
                })
            }
        } else {
            UIView.animate(withDuration: 0.5,
                           animations: { self.view.alpha = 0 },
                           completion: { _ in
                            self.activityIndicator.stopAnimating()
                            self.view.removeFromSuperview()
            })
        }
    }
    
    var busy: Bool {
        if view.superview != nil {
            return true
        }
        return false
    }
}
