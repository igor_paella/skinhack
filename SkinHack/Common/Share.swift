//
//  Share.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import SafariServices

class Share: NSObject, MFMailComposeViewControllerDelegate, SFSafariViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    static let sharedInstance = Share()
    
    private var messageCallback: ((Bool) -> Void)?
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
        Router.top.context?.present(alert, animated: true, completion: nil)
    }
    
    func openUrl(_ url: URL, vc: UIViewController) {
        let svc = SFSafariViewController(url: url)
        svc.delegate = self
        vc.present(svc, animated: true, completion: nil)
    }
    
    func sendEmail(_ text: String, subject: String, to: String, vc: UIViewController, configurate: ((MFMailComposeViewController) -> Void)? = nil) {
        
        if MFMailComposeViewController.canSendMail() {
            let composerVC = MFMailComposeViewController()
            composerVC.mailComposeDelegate = self
            composerVC.setToRecipients([to])
            composerVC.setSubject(subject)
            composerVC.setMessageBody(text, isHTML: false)
            configurate?(composerVC)
            vc.present(composerVC, animated: true, completion: nil)
        }
    }
    
    func sendMessage(subject: String?, body: String, recipients: [String], vc: UIViewController?, callback: @escaping (Bool) -> Void) {
        if MFMessageComposeViewController.canSendText() {
            messageCallback = callback
            let controller = MFMessageComposeViewController()
            controller.subject = subject
            controller.body = body
            controller.recipients = recipients
            controller.messageComposeDelegate = self
            vc?.present(controller, animated: true, completion: nil)
        }
    }
    
    // MARK: SFSafariViewControllerDelegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: MFMessageComposeViewControllerDelegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
        messageCallback?(result == .sent)
        messageCallback = nil
    }
}
