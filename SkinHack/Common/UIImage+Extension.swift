//
//  UIImage+Extension.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

extension UIImage {
    func toAttributedString(_ font: UIFont? = nil) -> NSMutableAttributedString {
        let iconAttachment = NSTextAttachment()
        iconAttachment.image = self
        if let font = font {
            let mid = font.descender + font.capHeight
            iconAttachment.bounds = CGRect(x: 0, y: font.descender - self.size.height / 2 + mid + 2, width: self.size.width, height: self.size.height).integral
        }
        let attributedString = NSMutableAttributedString(attributedString: NSAttributedString(attachment: iconAttachment))
        return attributedString
    }
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
}
