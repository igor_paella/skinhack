//
//  DispatchQueue+Extension.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

extension DispatchQueue {
    func asyncChain(semaphore: DispatchSemaphore, request: @escaping ((() -> Void)?) -> Void) {
        self.async {
            _ = semaphore.wait(timeout: .now() + 60)
            request {
                semaphore.signal()
            }
        }
    }
}

func delay(_ delay: Double, closure: @escaping () -> Void) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
