//
//  SlideTransition.swift
//  Ward
//
//  Created by Игорь on 08/11/2017.
//  Copyright © 2017 ward. All rights reserved.
//

import Foundation

class SlideTransition: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning {
    
    var interactionInProgress = false
    var reverse: Bool = false
    var swipeToPopEnabled = false
    weak var navVC: UINavigationController? {
        didSet {
            let recognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(SlideTransition.pan(recognizer:)))
            recognizer.edges = .left
            navVC?.view.addGestureRecognizer(recognizer)
        }
    }
    private let durationTime: TimeInterval = 0.4
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return durationTime
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to) else { return }
        let containerView = transitionContext.containerView
        let direction: CGFloat = reverse ? -1 : 1
        toVC.view.frame = CGRect(x: direction * containerView.frame.width, y: 0, width: containerView.frame.width, height: containerView.frame.height)
        containerView.addSubview(fromVC.view)
        containerView.addSubview(toVC.view)
        
        UIView.animate(withDuration: durationTime, delay: 0, options: .curveEaseInOut, animations: {
            toVC.view.frame = CGRect(x: 0, y: 0, width: containerView.frame.width, height: containerView.frame.height)
            fromVC.view.frame = CGRect(x: -direction * containerView.frame.width, y: 0, width: containerView.frame.width, height: containerView.frame.height)
            
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            if transitionContext.transitionWasCancelled {
                toVC.view.removeFromSuperview()
            } else {
                fromVC.view.removeFromSuperview()
            }
        })
    }
    
    @objc private func pan(recognizer: UIScreenEdgePanGestureRecognizer) {
        guard swipeToPopEnabled == true else { return }
        let translation = recognizer.translation(in: recognizer.view!.superview!)
        var progress = (translation.x / recognizer.view!.frame.width)
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
        
        switch recognizer.state {
        case .began:
            interactionInProgress = true
            _ = navVC?.popViewController(animated: true)
        case .changed:
            update(progress)
        case .cancelled:
            interactionInProgress = false
            cancel()
        case .ended:
            interactionInProgress = false
            if progress < 0.5 {
                cancel()
            } else {
                finish()
            }
        default:
            break
        }
    }
}
