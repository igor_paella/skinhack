//
//  UIImageView+Extension.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

extension UIImageView {
    
    func setImageByUrl(_ urlString: String?, placeholderImage: UIImage? = nil, clear: Bool = true, completion: ((UIImage?, Bool) -> Void)? = nil) {
        if let placeholderImage = placeholderImage {
            self.image = placeholderImage
        } else if clear || urlString == nil {
            self.image = nil
        }
        
        if urlString?.hasPrefix("http") == true {
            if let urlString = urlString,
                let url = URL(string: urlString) {
                self.pin_setImage(from: url, placeholderImage: placeholderImage, completion: { res in
                    
                    completion?(res.image, res.resultType == .cache || res.resultType == .memoryCache)
                })
            } else {
                self.image = placeholderImage
            }
        } else {
            if let urlString = urlString, !urlString.isEmpty {
                image = UIImage(named: urlString)
            }
        }
    }
}

extension UIButton {
    func setImageByUrl(_ urlString: String?, clear: Bool = true) {
        if urlString?.hasPrefix("http") == true {
            if clear {
                setImage(nil, for: .normal)
            }
            if let urlString = urlString,
                let url = URL(string: urlString) {
                self.pin_setImage(from: url)
            }
        } else {
            setImage(UIImage(named: urlString ?? ""), for: .normal) // from assets
        }
    }
}
