//
//  Colors.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class Colors {
//    class var gray: UIColor {
//        return UIColor(netHex: 0xD8D8D8)
//    }

    class var calendarCell: UIColor {
        return UIColor(netHex: 0xD8D8D8, alpha: 0.1)
    }

    class var calendarCellToday: UIColor {
        return UIColor(netHex: 0x2DEFFF, alpha: 0.3)
    }

    class var pink: UIColor {
        return UIColor(netHex: 0xFF2D92, alpha: 1.0)
    }
    
    class var cyan: UIColor {
        return UIColor(netHex: 0x2DEFFF, alpha: 1.0)
    }
    
    class var gray: UIColor {
        return UIColor(netHex: 0x2F2F2F, alpha: 1.0)
    }

    class var blue: UIColor {
        return UIColor(netHex: 0x2B87FF)
    }
    
    class var background: UIColor {
        return UIColor(netHex: 0x2F2F2F)
    }
    
    class var black: UIColor {
        return UIColor.black
    }
    
    class var white: UIColor {
        return UIColor.white
    }
    
    class var clear: UIColor {
        return UIColor.clear
    }
    
    // MARK: - IMAGES funcs
    
    class func imageFromColor(_ color: UIColor) -> UIImage {
        return imageFromColor(color, width: 1, height: 1)
    }
    
    class func imageFromColor(_ color: UIColor, width: CGFloat, height: CGFloat) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
