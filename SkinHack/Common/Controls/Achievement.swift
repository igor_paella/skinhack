//
//  Achievement.swift
//  SkinHack
//
//  Created by Ivan Starchenkov on 11/26/17.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class Achievement: UIView {
 
    private var glow: UIImageView!
    private var view: UIView!
    private var image: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = Colors.clear
        
        glow = UIImageView(frame: bounds)
        addSubview(glow)
        
        view = UIView(frame: CGRect(x: 0, y: 0, width: 180, height: 180))
        view.center = CGPoint(x: screenWidth/2.0, y: screenWidth/2.0)
        view.layer.cornerRadius = 90
        view.clipsToBounds = true
        addSubview(view)
        
        image = UIImageView(frame: bounds)
        image.contentMode = .center
        addSubview(image)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut, .repeat, .autoreverse], animations: {
            self.glow.alpha = 0.75
        }, completion: { _ in
        })
    }
    
    func configurate(product: Product) {
        image.image = product.image!
        view.backgroundColor = Colors.pink
        glow.image = UIImage(named: "glow-pink")
    }
    
    func configurateHydrate() {
        image.image = UIImage(named: "glass")
        view.backgroundColor = Colors.cyan
        glow.image = UIImage(named: "glow-cyan")
    }
}
