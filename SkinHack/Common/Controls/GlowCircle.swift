//
//  GlowCircle.swift
//  SkinHack
//
//  Created by Anton M on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class GlowCircleView: BaseView {
    var textLabel: UILabel!
    var color: UIColor = UIColor(netHex: 0xC7FC8A)
    var selectable: Bool = true
    var imageView: UIImageView!
    var nextImageView: UIImageView!
    var touched: (() -> Void)?
    
    private var progressCircle = CAShapeLayer()
    private var fullCircle = CAShapeLayer()
    private var circleView: UIView!
    private var strokeCompletion: CGFloat = 1.0
    
    private(set) var shadowIsHidden = true
    
    init(color: UIColor, strokeCompletion: CGFloat = 1.0) {
        super.init(frame: .zero)
        self.color = color
        self.strokeCompletion = strokeCompletion
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupView() {
        super.setupView()
        
        imageView = UIImageView()
        imageView.contentMode = .center
        addSubview(imageView)
        
        nextImageView = UIImageView()
        nextImageView.contentMode = .center
        addSubview(nextImageView)
        
        circleView = UIView()
        
        circleView.layer.shadowColor = color.cgColor
        circleView.layer.shadowRadius = 10
        circleView.layer.shadowOffset = .zero
        circleView.layer.shadowOpacity = 1
        
        progressCircle = CAShapeLayer()
        progressCircle.strokeColor = color.cgColor
        progressCircle.fillColor = Colors.clear.cgColor
        progressCircle.lineWidth = 8.0
        progressCircle.lineCap = kCALineCapRound
        progressCircle.strokeEnd = strokeCompletion
        progressCircle.strokeStart = 0.0
        circleView.layer.addSublayer(progressCircle)
        
        fullCircle = CAShapeLayer()
        fullCircle.strokeColor = Colors.white.withAlphaComponent(0.1).cgColor
        fullCircle.fillColor = Colors.clear.cgColor
        fullCircle.lineWidth = 8.0
        fullCircle.lineCap = kCALineCapRound
        fullCircle.strokeEnd = 1.0
        fullCircle.strokeStart = 0.0
        circleView.layer.addSublayer(fullCircle)

        addSubview(circleView)
    }
    
    func startGlowAnimation() {
        let shadowOpacity: Float = shadowIsHidden ? 0.2 : 1
        let animation = CABasicAnimation(keyPath: "shadowOpacity")
        animation.fromValue = circleView.layer.shadowOpacity
        animation.toValue = shadowOpacity
        animation.duration = 0.75
        animation.autoreverses = true
        animation.repeatCount = .greatestFiniteMagnitude
        circleView.layer.add(animation, forKey: animation.keyPath)
        circleView.layer.shadowOpacity = shadowOpacity
        shadowIsHidden = !shadowIsHidden
    }
    
    func stopAnimation() {
        circleView.layer.removeAllAnimations()
    }
    
    func setWithoutAnimation(strokeStart: CGFloat, strokeEnd: CGFloat) {
        progressCircle.strokeStart = strokeStart
        progressCircle.strokeEnd = strokeEnd
    }
    
    func set(strokeStart: CGFloat, strokeEnd: CGFloat, clockwise: Bool = true, duration: TimeInterval = 0.75) {
        let animationStart = CABasicAnimation(keyPath: "strokeStart")
        animationStart.fromValue = progressCircle.strokeStart
        animationStart.toValue = strokeStart
        animationStart.duration = duration
        animationStart.fillMode = kCAFillModeBackwards
        animationStart.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animationStart.isRemovedOnCompletion = false
        progressCircle.strokeStart = strokeStart
        progressCircle.add(animationStart, forKey: "animStart")
        
        let animationEnd = CABasicAnimation(keyPath: "strokeEnd")
        animationEnd.fromValue = progressCircle.strokeEnd
        animationEnd.toValue = strokeEnd
        animationEnd.duration = duration
        animationEnd.fillMode = kCAFillModeForwards
        animationEnd.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animationEnd.isRemovedOnCompletion = false
        progressCircle.strokeEnd = strokeEnd
        progressCircle.add(animationEnd, forKey: "animEnd")
    }
    
    func set(image: UIImage?, color: UIColor) {
//        set(strokeEnd: 0)
        delay(0.5) { [weak self] in
            self?.color = color
            self?.circleView.layer.shadowColor = color.cgColor
            self?.progressCircle.strokeColor = color.cgColor
        }
        
        nextImageView.image = image
        nextImageView.alpha = 0
        
        animateChangeImage(completed: nil)
    }
    
    func animateChangeImage(completed: (() -> Void)?) {
        nextImageView.transform = CGAffineTransform.init(rotationAngle: 180 * .pi / 180.0)
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.35,
                       initialSpringVelocity: 0.7,
                       options: [ .curveEaseOut ],
                       animations: { [weak self] in
                self?.imageView.transform = CGAffineTransform.init(rotationAngle: 180.0 * .pi / 180.0)
                self?.nextImageView.transform = .identity
            }, completion: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.imageView.image = strongSelf.nextImageView.image
                strongSelf.nextImageView.image = nil
                strongSelf.imageView.alpha = 1
                strongSelf.imageView.transform = .identity
                strongSelf.nextImageView.transform = .identity
                completed?()
        })
        
        UIView.animate(withDuration: 0.3, animations: {
            self.imageView.alpha = 0
            self.nextImageView.alpha = 1
        })
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(ovalIn: bounds)
        progressCircle.path = path.cgPath
        fullCircle.path = path.cgPath
        
        imageView.frame = bounds
        nextImageView.frame = imageView.bounds
        
        circleView.frame = bounds
        circleView.layer.cornerRadius = circleView.bounds.width / 2
        circleView.transform = CGAffineTransform(rotationAngle: 3 * CGFloat.pi / 2)

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard selectable else { return }
        //circleView.layer.removeAllAnimations()
        alpha = 0.5
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        guard selectable else { return }
        alpha = 1
        touched?()
        //startGlowAnimation()
    }
}
