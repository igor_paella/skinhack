//
//  SelectCell.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class SelectCell: UICollectionViewCell {
    private var labelView: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        labelView = UILabel()
        labelView.numberOfLines = 1
        labelView.font = Fonts.Display.black(36)
        labelView.textColor = UIColor(netHex: 0xE4FFC4).withAlphaComponent(0.27)
        contentView.addSubview(labelView)

        constrain(labelView) { labelView in
            labelView.edges == labelView.superview!.edges
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configurate(item: SelectItem) {
        labelView.text = "\(item.value)"
    }
}
