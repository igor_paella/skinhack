//
//  SelectView.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//
// swiftlint:disable force_cast

import Foundation
import Cartography

class SelectItem {
    var id: String
    var value: Int
    
    init(id: String, value: Int) {
        self.id = id
        self.value = value
    }
}

class SelectView: BaseView, UICollectionViewDelegate, UICollectionViewDataSource {
    static let height: CGFloat = 150
    var valueDesc: String = ""
    
    private let cellIdent = "ItemCell"
    private var collectionView: UICollectionView!
    private var centerView: UILabel!
    private var glowView: GlowCircleView!
    private var items: [SelectItem] = []
    private(set) var selectedItem: SelectItem?
    private let itemWidth: CGFloat = 75
    
    var touched: ((SelectItem) -> Void)?
    
    override func setupView() {
        super.setupView()
        
        backgroundColor = Colors.clear
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 10
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let sideOffset = (UIScreen.main.bounds.width - itemWidth)/2
        collectionView = UICollectionView(frame: self.bounds, collectionViewLayout: layout)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: sideOffset, bottom: 0, right: sideOffset)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SelectCell.self, forCellWithReuseIdentifier: cellIdent)
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.clipsToBounds = false
        addSubview(collectionView)
        
        centerView = UILabel()
        centerView.isUserInteractionEnabled = false
        centerView.textAlignment = .center
        centerView.layer.cornerRadius = 75
        centerView.layer.masksToBounds = true
        centerView.backgroundColor = Colors.background
        centerView.textColor = Colors.white
        centerView.font = Fonts.Display.black(50)
        centerView.numberOfLines = 2
        addSubview(centerView)
        
        glowView = GlowCircleView(color: Colors.white, strokeCompletion: 1)
        glowView.isUserInteractionEnabled = false
        addSubview(glowView)
        
        constrain(collectionView, centerView, glowView) { collectionView, centerView, glowView in
            collectionView.superview!.height == SelectView.height
            collectionView.edges == collectionView.superview!.edges
            
            centerView.width == 150
            centerView.height == 150
            centerView.centerX == centerView.superview!.centerX
            centerView.centerY == centerView.superview!.centerY + 7
            
            glowView.size == centerView.size
            glowView.center == glowView.superview!.center
        }
    }
    
    func startAnimate() {
        glowView.startGlowAnimation()
    }
    
    func update(items: [SelectItem]) {
        self.items = items
        collectionView.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    private func getItem(indexPath: IndexPath) -> SelectItem {
        return items[indexPath.row]
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = getItem(indexPath: indexPath)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdent, for: indexPath) as! SelectCell
        cell.configurate(item: item)
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let item = getItem(indexPath: indexPath)
//        selectedItem = item
//        //        var indexes: [IndexPath] = []
//        //        indexes.append(indexPath)
//        //        if let selectedIndexPath = selectedIndexPath {
//        //            indexes.append(selectedIndexPath)
//        //        }
//        collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems)
//        touched?(item)
//    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: itemWidth, height: SelectView.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = centerIndex(contentOffsetX: scrollView.contentOffset.x)
        let item = items[index]
        selectedItem = item
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.minimumLineHeight = 42
        paragraphStyle.maximumLineHeight = 42
        
        centerView.attributedText = NSAttributedString(string: "\(item.value)\n\(valueDesc)", attributes: [
            .paragraphStyle: paragraphStyle
        ])
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = centerIndex(contentOffsetX: targetContentOffset.pointee.x)
        let targetX = contentOffsetX(index: index)
        targetContentOffset.pointee.x = targetX
    }
    private func centerIndex(contentOffsetX: CGFloat) -> Int {
        let offsetX = contentOffsetX + frame.width/2 - (itemWidth)/2
        return min(max(0, Int(round(offsetX/(itemWidth + 10)))), items.count - 1)
    }
    private func contentOffsetX(index: Int) -> CGFloat {
        return CGFloat(index)*(itemWidth + 10) - frame.width/2 + (itemWidth)/2
    }
}
