//
//  ConstraintPriority.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

enum ConstraintPriority: Float {
    case max = 1000
    case medium = 500
}
