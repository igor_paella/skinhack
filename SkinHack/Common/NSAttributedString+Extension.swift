//
//  NSAttributedString+Extension.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

extension NSMutableAttributedString {
    func addImg(_ img: UIImage, font: UIFont? = nil, index: Int? = nil) {
        let string = NSMutableAttributedString()
        string.append(img.toAttributedString(font))
        if let font = font {
            string.append(NSAttributedString(string: " ", attributes: [NSAttributedStringKey.font: font]))
        }
        
        if let index = index {
            self.insert(string, at: index)
        } else {
            self.append(string)
        }
    }
}

extension NSAttributedString {
    func addParagraph(aligment: NSTextAlignment, lineSpacing: CGFloat? = nil) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = aligment
        if let lineSpacing = lineSpacing {
            paragraphStyle.lineSpacing = lineSpacing
        }
        let mutableString = NSMutableAttributedString(attributedString: self)
        mutableString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: mutableString.length))
        return mutableString
    }
}
