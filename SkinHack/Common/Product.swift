//
//  Product.swift
//  SkinHack
//
//  Created by Ivan Starchenkov on 11/25/17.
//  Copyright © 2017 xuli. All rights reserved.
//
// swiftlint:disable cyclomatic_complexity

import Foundation

enum Product: String, Codable {
    case carrot // 33kkal
    case watermellon // 38kkal
    case apple // 46kkal
    case banan // 91kkal
    case salad // 130kkal
    case chicken // 246kkal
    case pizza // 300kkal
    case donut // 400kkal
    case burger // 503kkal
    case burgerWithFries = "burger+fries" // 503 + 231 = 734kkal
    case burgerWithFriesAndCoke = "burger+fries+coke" // 503 + 231 + 106 = 840kkal
    
    init?(calories: Float) {
        if calories >= Product.burgerWithFriesAndCoke.callories() {
            self.init(rawValue: "burger+fries+coke")
        } else if calories >= Product.burgerWithFries.callories() {
            self.init(rawValue: "burger+fries")
        } else if calories >= Product.burger.callories() {
            self.init(rawValue: "burger")
        } else if calories >= Product.donut.callories() {
            self.init(rawValue: "donut")
        } else if calories >= Product.pizza.callories() {
            self.init(rawValue: "pizza")
        } else if calories >= Product.chicken.callories() {
            self.init(rawValue: "chicken")
        } else if calories >= Product.salad.callories() {
            self.init(rawValue: "salad")
        } else if calories >= Product.banan.callories() {
            self.init(rawValue: "banan")
        } else if calories >= Product.apple.callories() {
            self.init(rawValue: "apple")
        } else if calories >= Product.watermellon.callories() {
            self.init(rawValue: "watermellon")
        } else {
            self.init(rawValue: "carrot")
        }
    }
    
    func callories() -> Float {
        switch self {
        case .carrot:
            return 20
        case .watermellon:
            return 30
        case .apple:
            return 50
        case .banan:
            return 80
        case .salad:
            return 120
        case .chicken:
            return 180
        case .pizza:
            return 250
        case .donut:
            return 350
        case .burger:
            return 500
        case .burgerWithFries:
            return 700
        case .burgerWithFriesAndCoke:
            return 1000
        }
    }
    
    func name() -> String {
        switch self {
        case .carrot:
            return "Carrot"
        case .watermellon:
            return "Watermellon"
        case .apple:
            return "Apple"
        case .banan:
            return "Banan"
        case .salad:
            return "Salad"
        case .chicken:
            return "Chicken"
        case .pizza:
            return "Pizza"
        case .donut:
            return "Donut"
        case .burger:
            return "Burger"
        case .burgerWithFries:
            return "Burger with fries"
        case .burgerWithFriesAndCoke:
            return "Burger, fries and coke"
        }
    }
    
    var image: UIImage? {
        return UIImage(named: self.rawValue)
    }
    
    var color: UIColor {
        return UIColor(netHex: 0xFFD57F)
    }
}
