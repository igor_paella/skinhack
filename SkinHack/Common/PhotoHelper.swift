//
//  PhotoHelper.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class PhotoHelper: NSObject {
    
    static let sharedInstance = PhotoHelper()
    
    fileprivate var didSelect: ((UIImage?) -> Void)?
    
    func showImagePicker(_ source: UIImagePickerControllerSourceType, vc: UIViewController, didSelect: @escaping ((UIImage?) -> Void)) {
        self.didSelect = didSelect
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let cameraController = UIImagePickerController()
            cameraController.delegate = self
            cameraController.sourceType = source
            vc.present(cameraController, animated: true) {}
        }
    }
    
    class func cropToBounds(image: UIImage, bounds: CGRect) -> UIImage {
        let contextImage: UIImage = image.fixedOrientation()
        let imageRef = contextImage.cgImage!.cropping(to: bounds)
        let result: UIImage = UIImage(cgImage: imageRef!, scale: image.scale, orientation: .up)
        return result
    }
}

extension PhotoHelper: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String: AnyObject]?) {
        didSelect?(image)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        didSelect?(nil)
        picker.dismiss(animated: true, completion: nil)
    }
}

extension UIImage {
    
    func fixedOrientation() -> UIImage {
        
        if imageOrientation == .up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi/2)
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: -CGFloat.pi/2)
        case .up, .upMirrored:
            break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: (self.cgImage?.colorSpace)!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        return UIImage(cgImage: cgImage)
    }
}

extension UIImage {
    func resizeWith(width: CGFloat) -> UIImage? {
        let resultSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(resultSize, false, 0)
        self.draw(in: CGRect(origin: CGPoint.zero, size: resultSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
    
    func convertToBase64() -> String? {
        let imageData = UIImageJPEGRepresentation(self, 1)
        return imageData?.base64EncodedString(options: [])
    }
}
