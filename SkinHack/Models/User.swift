//
//  User.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class User: Codable, JSON {
    var id: String = ""
    var name: String?
    var avatar: String?
    var weight: Int?
    var height: Int?
    
    init(id: String, name: String?, avatar: String?, weight: Int?, height: Int?) {
        self.id = id
        self.name = name
        self.avatar = avatar
        self.weight = weight
        self.height = height
    }
}

class LeaderboardItem: Codable, JSON {
    var id: String = ""
    var userId: String?
    var name: String?
    var avatar: String?
    var res: Product?
    var result: Double = 0.0
    var date: Date?
    var mmdd: String = ""
    
    init(id: String, userId: String?, name: String?, avatar: String?, res: Product?, result: Double, date: Date?) {
        self.id = id
        self.userId = userId
        self.name = name
        self.avatar = avatar
        self.res = res
        self.result = result
        self.date = date
    }
    
    func beforeEncode() {
        mmdd = date?.mmdd ?? ""
    }
}

extension User {
    var personCoeff: Double? {
        if let weight = weight,
            let height = height {
            return 100/Double(abs(height - weight))
        }
        return nil
    }
}
