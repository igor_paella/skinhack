//
//  JSON.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol JSON {
    func json() -> [String: Any]?
    func beforeEncode()
}

func decode<T: JSON & Codable>(_ type: T.Type, json: [String: Any]) -> T? {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .iso8601
    if let data = try? JSONSerialization.data(withJSONObject: json, options: []),
        let object = try? decoder.decode(type, from: data) {
        return object
    }
    return nil
}

extension JSON where Self: Codable {
    func json() -> [String: Any]? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        self.beforeEncode()
        if let jsonData = try? encoder.encode(self),
            let object = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) {
            return object as? [String: Any]
        }
        return nil
    }
    
    func beforeEncode() {
        
    }
}
