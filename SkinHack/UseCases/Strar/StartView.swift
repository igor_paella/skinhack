//
//  StartView.swift
//  SkinHack
//
//  Created by Anton M on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class StartView: BaseView {
    
    var titleTopConstraint: NSLayoutConstraint!
    var descriptionTopConstraint: NSLayoutConstraint!
    
    let circleView: GlowCircleView = {
        let view = GlowCircleView(color: UIColor(netHex: 0xC7FC8A))
        view.imageView.image = #imageLiteral(resourceName: "START")
        return view
    }()
    
    let disconnectButton: UIButton = {
        let view = UIButton()
        view.titleLabel?.font = Fonts.Display.regular(18)
        view.setTitleColor(Colors.white, for: .normal)
        view.setTitleColor(Colors.white.withAlphaComponent(0.5), for: .highlighted)

        view.setTitle("disconnect", for: .normal)
        return view
    }()
    
    let timeLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.monospacedDigitSystemFont(ofSize: 36, weight: .black)//Fonts.Display.black(36)
        view.textColor = Colors.white
        view.alpha = 0
        view.text = "30:00"
        view.numberOfLines = 0
        view.textAlignment = .center
        return view
    }()
    
    let timerTitleLabel: UILabel = {
        let view = UILabel()
        view.font = Fonts.Display.black(16)
        view.textColor = Colors.white
        view.text = "remaining:"
        view.alpha = 0
        view.textAlignment = .center
        return view
    }()
    
    let button: UIButton = {
        let view = UIButton()
        view.setTitleColor(Colors.white, for: .normal)
        view.setTitle("", for: .normal)
        view.titleLabel?.font = Fonts.Display.black(36)
        view.alpha = 0        
        return view
    }()
    
    let achievemnt: Achievement = {
        let view = Achievement(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenWidth))
        view.center = CGPoint(x: screenWidth/2.0, y: screenHeight/2.0)
        view.alpha = 0
        return view
    }()
    
    let calendarButton: UIButton = {
        let view = UIButton()
        view.adjustsImageWhenHighlighted = false
        view.setImage(UIImage(named: "button-calendar"), for: .normal)
        return view
    }()

    let leaderboardButton: UIButton = {
        let view = UIButton()
        view.adjustsImageWhenHighlighted = false
        view.setImage(UIImage(named: "button-leaderboard"), for: .normal)
        return view
    }()

    override func setupView() {
        super.setupView()
        backgroundColor = Colors.background
        
        addSubview(achievemnt)
        addSubview(circleView)
        addSubview(disconnectButton)
        addSubview(timerTitleLabel)
        addSubview(timeLabel)
        addSubview(button)
        addSubview(calendarButton)
        addSubview(leaderboardButton)
        
        constrain(calendarButton, leaderboardButton) { calendarButton, leaderboardButton in
            calendarButton.width == 95
            calendarButton.height == 75
            calendarButton.bottom == calendarButton.superview!.bottom - 50
            calendarButton.left == calendarButton.superview!.left + 50
            
            leaderboardButton.width == 95
            leaderboardButton.height == 75
            leaderboardButton.bottom == leaderboardButton.superview!.bottom - 50
            leaderboardButton.right == leaderboardButton.superview!.right - 50
        }
        
        constrain(circleView, disconnectButton, timerTitleLabel, timeLabel, button) { circleView, disconnectButton, timerTitleLabel, timeLabel, button in
            circleView.centerX == circleView.superview!.centerX
            circleView.centerY == circleView.superview!.centerY
            circleView.width == circleView.height
            circleView.height == 225
            
            disconnectButton.top == disconnectButton.superview!.top + 30
            disconnectButton.right == disconnectButton.superview!.right - 20
            
            self.descriptionTopConstraint = timerTitleLabel.top == timerTitleLabel.superview!.top + 51
            timerTitleLabel.left == timerTitleLabel.superview!.left
            timerTitleLabel.right == timerTitleLabel.superview!.right
            
            self.titleTopConstraint = timeLabel.top == timeLabel.superview!.top + 70
            timeLabel.left == timeLabel.superview!.left
            timeLabel.right == timeLabel.superview!.right
            
            button.bottom == button.superview!.bottom - 70
            button.left == button.superview!.left
            button.right == button.superview!.right
            button.height == 60
        }
    }
}
