//
//  StartViewModel.swift
//  SkinHack
//
//  Created by Anton M on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol StartViewModelProtocol: BaseViewModelProtocol {
    func startWorkOut()
    func disconnect()
    func workoutCompleted()
    func dissapear()
    
    var productChanged: ((Product) -> Void)? { get set }
    var strokeChanged: ((CGFloat, Bool) -> Void)? { get set }

    var workoutDoneTitle: String { get }
    var workoutDoneDescription: String { get }
    var hydrationDescription: String { get }
    var workoutDone: Bool { get set }
    var hydrationDone: Bool { get set }
    var achievedProduct: Product? { get set }
    var glasses: Int { get }
}

class StartViewModel: BaseViewModel, StartViewModelProtocol {
    var tshirtFacade: TShirtFacadeProtocol!
    var userFacade: UserFacadeProtocol!
    
    var strokeChanged: ((CGFloat, Bool) -> Void)?
    var productChanged: ((Product) -> Void)?
    var currentProduct: Product!
    var achievedProduct: Product?
    
    private var kkalLevel: CGFloat = 0
    private var kkal: CGFloat = 0
    private let products: [Product] = [.carrot, .watermellon, .apple, .banan, .salad, .chicken, .pizza, .donut, .burger, .burgerWithFries, .burgerWithFriesAndCoke]

    var workoutDoneTitle: String {
        switch kkal {
        case 0..<20:
            return "Couch potato"
        case 20..<30:
            return "Move on"
        case 30..<50:
            return "Not bad"
        case 50..<80:
            return "Try harder"
        case 80..<120:
            return "GOOD"
        case 180..<250:
            return "KEEP GOING"
        case 250..<350:
            return "WELL DONE!"
        case 350..<500:
            return "PERFECT"
        case 500..<700:
            return "INTENSE"
        case 700...:
            return "SUPERB!"
        default:
            break
        }
        return ""
    }
    var workoutDoneDescription: String {
        switch kkal {
        case 0..<20:
            return "You have to exercise more"
        case 20..<30:
            return "How is about another 30 mins?"
        case 30..<50:
            return "How is about another 30 mins?"
        case 50..<80:
            return "Get sweat baby!"
        case 80..<120:
            return "Continue that way"
        case 180..<250:
            return "Continue that way"
        case 250..<350:
            return "You've burn at new level!"
        case 350..<500:
            return "It was near perfect"
        case 500..<700:
            return "I bet you are pro?!"
        case 700..<5000:
            return "Are you human or what?"
        default:
            break
        }
        return ""
    }
    
    var hydrationDescription: String {
        return "Drink at least \(max(1, min(15, glasses))) glass of water"
    }
    var workoutDone = false
    var hydrationDone = false
    var glasses: Int {
        return Int((kkal * 1.7) / 250.0 + 0.5)
    }

    override func didLoad() {
        currentProduct = products[0]
        userFacade.getUser { [weak self] user in
            if let coeff = user?.personCoeff {
                self?.tshirtFacade.setup(personCoeff: coeff)
            }
        }
    }
    
    private func checkProduct() {
        
    }
    
    func startWorkOut() {
        tshirtFacade.stopFlashing()
        productChanged?(currentProduct)
        tshirtFacade.startReadSensors { [weak self] value in
            self?.kkal = CGFloat(value)
            self?.updateKkal()
        }
    }
    
    private func updateKkal() {
        var newProduct = false
        if kkal > CGFloat(currentProduct.callories()) {
            if let index = products.index(of: currentProduct),
                index < (products.count - 1) {
                kkalLevel = CGFloat(currentProduct.callories())
                achievedProduct = currentProduct
                currentProduct = products[index + 1]
                newProduct = true
                productChanged?(currentProduct)
            }
        }
        
        let stroke = (kkal - kkalLevel)/(CGFloat(currentProduct.callories()) - kkalLevel)
        strokeChanged?(stroke, newProduct)
    }
    
    func disconnect() {
        tshirtFacade.disconnect(completed: { [weak self] in
            self?.router.deviceList().replaceRoot(animated: true, completion: nil)
        })
    }
    
    func workoutCompleted() {
        tshirtFacade.stopReadSensors()
        userFacade.addWorkout(kkal: Double(kkal)) { _ in }
    }
    
    func dissapear() {
        tshirtFacade.stopReadSensors()
    }
}
