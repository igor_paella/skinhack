//
//  StartViewController.swift
//  SkinHack
//
//  Created by Anton M on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class StartViewController: BaseViewController<StartView>, ViewModelHolder {
    
    typealias TViewModel = StartViewModelProtocol
    private var workOutTimer: Timer?
    private var workOutStartDate: Date?
    let workOutDuration: Double = 30 * 60
    private let texts: [String] = ["WARM UP!", "GO HARDER", "KEEP GOING", "AWESOME!", "HARDER!", "ALMOST THERE"]
    
    override func setupView() {
        super.setupView()

        navigationController?.isNavigationBarHidden = true
        baseView.disconnectButton.addTarget(self, action: #selector(StartViewController.disconnectTouched), for: .touchUpInside)
        baseView.circleView.touched = { [weak self] in self?.startTouched() }
        baseView.button.addTarget(self, action: #selector(StartViewController.nextTouched), for: .touchUpInside)
        baseView.leaderboardButton.addTarget(self, action: #selector(StartViewController.openLeaderboard), for: .touchUpInside)
        baseView.calendarButton.addTarget(self, action: #selector(StartViewController.openCalendar), for: .touchUpInside)
        
        viewModel.productChanged = { [weak self] in self?.changed(product: $0) }
        viewModel.strokeChanged = { [weak self] strokeEnd, newProduct in self?.changed(strokeEnd: strokeEnd, newProduct: newProduct) }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        baseView.circleView.startGlowAnimation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(StartViewController.willEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        viewModel.dissapear()
        NotificationCenter.default.removeObserver(self) // swiftlint:disable:this notification_center_detachment
    }
    
    @objc internal func willEnterForeground(_ notification: Notification) {
        baseView.circleView.startGlowAnimation()
    }
    
    func startTouched() {
        UIView.animate(withDuration: 0.3) {
            self.baseView.leaderboardButton.alpha = 0
            self.baseView.calendarButton.alpha = 0
            self.baseView.disconnectButton.alpha = 0
            self.baseView.timeLabel.alpha = 1
            self.baseView.timerTitleLabel.alpha = 1
            self.baseView.button.alpha = 1
        }
        baseView.circleView.selectable = false
        
        startWorkOut()
    }
    
    @objc func disconnectTouched() {
        viewModel.disconnect()
    }
    
    func startWorkOut() {
        baseView.circleView.set(strokeStart: 0, strokeEnd: 0)
        viewModel.startWorkOut()
        
        workOutStartDate = Date()
        workOutTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] timer in
            let now = Date()
            if let start = self?.workOutStartDate, let duration = self?.workOutDuration {
                let diff = now.timeIntervalSince(start)
                if diff <= duration {
                    let remainingTime = duration - diff
                    self?.updateTitle(timeRange: diff)
                    let sec = Int(remainingTime.truncatingRemainder(dividingBy: 60))
                    let min = Int((remainingTime / 60).truncatingRemainder(dividingBy: 60))
                   
                    self?.baseView.timeLabel.text = "\(min):\(sec >= 10 ? "" : "0")\(sec)"
                } else {
                    self?.workoutOver()
                    timer.invalidate()
                }
            } else {
                timer.invalidate()
            }
        })
    }
    private func update(title: String) {
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.baseView.button.alpha = 0
        }, completion: { _ in
            self.baseView.button.setTitle(title, for: .normal)
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                self.baseView.button.alpha = 1
            }, completion: { _ in
            })
        })
    }
    
    private var currentRange = -1
    private func updateTitle(timeRange: Double) {
        var range: Int = 0
        switch timeRange {
        case 0..<300:
            range = 0
        case 300..<600:
            range = 1
        case 600..<900:
            range = 2
        case 900..<1200:
            range = 3
        case 1200..<1500:
            range = 4
        case 1500..<1800:
            range = 5
        default:
            break
        }
        if range != currentRange {
            currentRange = range
            update(title: texts[range])
        }
    }
    
    private func changed(product: Product) {
        baseView.circleView.set(image: product.image, color: product.color)
    }
    
    private func changed(strokeEnd: CGFloat, newProduct: Bool) {
        if newProduct {
            baseView.circleView.set(strokeStart: 0.999, strokeEnd: 0.9999, duration: sensorInterval/2)
            delay(sensorInterval/2) {
                self.baseView.circleView.setWithoutAnimation(strokeStart: 0, strokeEnd: 0)
                self.baseView.circleView.set(strokeStart: 0, strokeEnd: strokeEnd, duration: sensorInterval/2)
            }
        } else {
            baseView.circleView.set(strokeStart: 0, strokeEnd: strokeEnd, duration: sensorInterval)
        }
    }
    
    @objc func nextTouched() {
        if viewModel.workoutDone && !viewModel.hydrationDone {
            showHydration()
        } else if viewModel.workoutDone && viewModel.hydrationDone {
            viewModel.router.leaderboard(isModal: false).push()
        }
    }
    
    func workoutOver() {
        viewModel.workoutCompleted()
        viewModel.workoutDone = true
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.baseView.timerTitleLabel.alpha = 0
            self.baseView.timeLabel.alpha = 0
            self.baseView.button.alpha = 0
            self.baseView.circleView.alpha = 0
        }, completion: { _ in
            self.baseView.button.setTitle("NEXT", for: .normal)
            self.baseView.button.setTitleColor(Colors.pink, for: .normal)
            self.baseView.timeLabel.textColor = Colors.pink
            self.baseView.timeLabel.text = self.viewModel.workoutDoneTitle
            self.baseView.timerTitleLabel.text = self.viewModel.workoutDoneDescription
            self.baseView.titleTopConstraint.constant = 151
            self.baseView.descriptionTopConstraint.constant = 204
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                self.baseView.timerTitleLabel.alpha = 1
                self.baseView.timeLabel.alpha = 1
                self.baseView.button.alpha = 1
                self.baseView.layoutIfNeeded()
                self.baseView.achievemnt.alpha = 1
            }, completion: { _ in
            })

            if let achievedProduct = self.viewModel.achievedProduct {
                self.baseView.achievemnt.configurate(product: achievedProduct)
                self.baseView.achievemnt.transform = CGAffineTransform.init(scaleX: 0.1, y: 0.1)
                UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [.curveEaseInOut], animations: {
                    self.baseView.achievemnt.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
                }, completion: {_ in
                })
            }
        })
    }
    
    func showHydration() {
        viewModel.hydrationDone = true        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.baseView.timerTitleLabel.alpha = 0
            self.baseView.timeLabel.alpha = 0
            self.baseView.button.alpha = 0
            self.baseView.achievemnt.alpha = 0
        }, completion: { _ in
            self.baseView.button.setTitle("GOT IT!", for: .normal)
            self.baseView.button.setTitleColor(Colors.cyan, for: .normal)
            self.baseView.timeLabel.textColor = Colors.cyan
            self.baseView.timeLabel.text = "DON’T FORGET\nTO HYDRATE WELL"
            self.baseView.timerTitleLabel.text = self.viewModel.hydrationDescription
            self.baseView.titleTopConstraint.constant = 108
            self.baseView.descriptionTopConstraint.constant = 204
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                self.baseView.timerTitleLabel.alpha = 1
                self.baseView.timeLabel.alpha = 1
                self.baseView.button.alpha = 1
                self.baseView.layoutIfNeeded()
                self.baseView.achievemnt.alpha = 1
            }, completion: { _ in
            })
            self.baseView.achievemnt.configurateHydrate()
            self.baseView.achievemnt.transform = CGAffineTransform.init(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [.curveEaseInOut], animations: {
                self.baseView.achievemnt.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            }, completion: {_ in
            })
        })
    }
    
    @objc private func openCalendar() {
        viewModel.router.calendar().present()
    }
    
    @objc private func openLeaderboard() {
        viewModel.router.leaderboard(isModal: true).present()
    }
}
