//
//  AuthView.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography
import FBSDKLoginKit

class AuthView: BaseView {
    var title: UILabel!
    var loginButton: UIButton!

    override func setupView() {
        super.setupView()
        
        backgroundColor = Colors.background
        
        title = UILabel()
        title.textColor = Colors.white
        title.text = "Sign in"
        title.font = Fonts.Display.bold(24)
        addSubview(title)
        
        loginButton = UIButton()
        loginButton.setTitle("WITH FACEBOOK", for: .normal)
        loginButton.titleLabel?.font = Fonts.Display.heavy(36)
        loginButton.setTitleColor(Colors.blue, for: .normal)
        addSubview(loginButton)
        
        constrain(loginButton, title) { loginButton, title in
            title.top == title.superview!.top + 35
            title.centerX == title.superview!.centerX
            loginButton.center == loginButton.superview!.center
        }
    }
}
