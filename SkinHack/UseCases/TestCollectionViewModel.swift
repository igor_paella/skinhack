//
//  TestCollectionViewModel.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol TestCollectionViewModelProtocol: CollectionViewModelProtocol {
}

class TestCollectionViewModel: BaseCollectionViewModel, TestCollectionViewModelProtocol {

    private var string: String
    
    init(string: String) {
        self.string = string
    }
    
    override func update() {
        updated?([[
            SpaceCellViewModel(height: 100),
            TextCellViewModel(text: NSAttributedString(string: "test", attributes: [NSAttributedStringKey.foregroundColor: Colors.black]))
        ]], .firstLoad)
    }
    
    override func willAppear() {
        super.willAppear()
        
        update()
    }
}
    
