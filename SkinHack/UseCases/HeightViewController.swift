//
//  HeightViewController.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class HeightViewController: BaseViewController<HeightView>, ViewModelHolder {
    typealias TViewModel = HeightViewModelProtocol
    
    override func setupView() {
        super.setupView()
        
        baseView.nextButton.addTarget(self, action: #selector(HeightViewController.nextTouched), for: .touchUpInside)
        
        viewModel.busyChanged = { Activity.instance.busy($0, yOffset: 150) }
        viewModel.itemsChanged = { [weak self] in self?.baseView.selectView.update(items: $0) }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        baseView.selectView.startAnimate()
    }
    
    @objc private func nextTouched() {
        guard let item = baseView.selectView.selectedItem else { return }
        viewModel.next(height: item.value)
    }
}
