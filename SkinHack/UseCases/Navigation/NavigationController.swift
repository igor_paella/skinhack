//
//  NavigationController.swift
//  SkinHack
//
//  Created by Anton M on 26/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class NavigationController: UINavigationController, UINavigationControllerDelegate {
    
    private let transition = SlideTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        navigationBar.isHidden = true
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.reverse = operation == .pop
        transition.navVC = self
        return transition
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return transition.interactionInProgress ? transition : nil
    }
}
