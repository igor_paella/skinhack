//
//  WeightViewController.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class WeightViewController: BaseViewController<WeightView>, ViewModelHolder {
    typealias TViewModel = WeightViewModelProtocol
    
    override func setupView() {
        super.setupView()
        
        baseView.nextButton.addTarget(self, action: #selector(WeightViewController.nextTouched), for: .touchUpInside)
        
        viewModel.busyChanged = { Activity.instance.busy($0, yOffset: 150) }
        viewModel.itemsChanged = { [weak self] in self?.baseView.selectView.update(items: $0) }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        baseView.selectView.startAnimate()
    }
    
    @objc private func nextTouched() {
        guard let item = baseView.selectView.selectedItem else { return }
        viewModel.next(weight: item.value)
    }
}
