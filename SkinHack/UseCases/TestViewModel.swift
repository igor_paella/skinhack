//
//  TestViewModel.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol TestViewModelProtocol: BaseViewModelProtocol {
    func openCollection()
}

class TestViewModel: BaseViewModel, TestViewModelProtocol {
    
    var facade: TestFacadeProtocol!
    
    override func didLoad() {
        facade.test()
    }
    
    func openCollection() {
        router.collectionTest(string: "test param").push()
    }
}
