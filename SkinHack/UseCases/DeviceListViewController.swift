//
//  DeviceListViewController.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class DeviceListViewController: BaseCollectionViewController, ViewModelHolder {
    
    typealias TViewModel = DeviceListViewModelProtocol
    
    var titleView: UILabel!
    var indicator: UIActivityIndicatorView!
    
    override var cellType: CellsRegistrotor {
        return .test(self)
    }
    
    override func createEmptyView() -> UIView? {
        let view = UIView()
        let empty = UILabel()
        empty.textColor = Colors.white
        empty.font = Fonts.Display.regular(22)
        empty.text = "Can't find any T-Shirt :-("
        view.addSubview(empty)
        let button = UIButton()
        button.setTitle("Retry", for: .normal)
        button.setTitleColor(Colors.blue, for: .normal)
        button.titleLabel?.font = Fonts.Display.heavy(20)
        button.addTarget(self, action: #selector(DeviceListViewController.reloadList), for: .touchUpInside)
        view.addSubview(button)
        
        constrain(empty, button) { empty, button in
            empty.left == empty.superview!.left
            empty.right == empty.superview!.right
            empty.top == empty.superview!.top
            
            button.left == button.superview!.left
            button.right == button.superview!.right
            button.bottom == button.superview!.bottom
            button.top == empty.bottom + 20
        }
        return view
    }
    
    override func setupView() {
        super.setupView()
    
        view.backgroundColor = Colors.background
        
        titleView = UILabel()
        titleView.textColor = Colors.white
        titleView.text = "Connect to T-shirt"
        titleView.font = Fonts.Display.bold(24)
        collectionView.addSubview(titleView)
        
        indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        indicator.hidesWhenStopped = true
        view.addSubview(indicator)
        
        constrain(titleView, indicator) { titleView, indicator in
            titleView.bottom == titleView.superview!.bottom - 16
            titleView.centerX == titleView.superview!.centerX
            
            indicator.centerX == indicator.superview!.centerX
            indicator.centerY == indicator.superview!.centerY
        }
        
        collectionView.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 0, right: 0)
        collectionView.scrollIndicatorInsets = collectionView.contentInset
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.reply, target: self, action: #selector(DeviceListViewController.reloadList))
    }
    
    @objc private func reloadList() {
        viewModel.reload()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator.startAnimating()
    }
    
    override func updateItemsCompleted() {
        indicator.stopAnimating()
    }
}
