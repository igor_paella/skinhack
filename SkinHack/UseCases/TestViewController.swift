//
//  TestViewController.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class TestViewController: BaseViewController<TestView>, ViewModelHolder {
    
    typealias TViewModel = TestViewModelProtocol
    
    override func setupView() {
        super.setupView()
        
        baseView.button.addTarget(self, action: #selector(TestViewController.buttonTouched), for: .touchUpInside)
    }
    
    @objc private func buttonTouched() {
        viewModel.openCollection()
    }
}
