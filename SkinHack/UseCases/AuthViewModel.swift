//
//  AuthViewModel.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol AuthViewModelProtocol: BaseViewModelProtocol {
    func auth()
    
    var busyChanged: ((Bool) -> Void)? { get set }
}

class AuthViewModel: BaseViewModel, AuthViewModelProtocol {
    var authFacade: AuthFacadeProtocol!
    var userFacade: UserFacadeProtocol!
    
    var busyChanged: ((Bool) -> Void)?
    var done: ((Router) -> Void)?
    
    init(done: ((Router) -> Void)?) {
        self.done = done
    }
    
    override func didLoad() {
        
    }
    
    func auth() {
        guard let vc = router.context else { return }
        busyChanged?(true)
        authFacade.auth(vc: vc) { [weak self] loggedUser in
            if let user = loggedUser?.toUser() {
                self?.userFacade.update(user: user) { [weak self] success in
                    self?.busyChanged?(false)
                    if success {
                        self?.raiseDone()
                    }
                }
            } else {
                self?.busyChanged?(false)
            }
        }
    }
    
    private func raiseDone() {
        done?(router)
    }
}
