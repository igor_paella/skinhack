//
//  TestView.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography
import Hero

class TestView: BaseView {
    
    var button: UIButton!
    var heroView: UILabel!
    
    override func setupView() {
        super.setupView()
        
        backgroundColor = Colors.white
        
        button = UIButton()
        button.setTitle("Open collection", for: .normal)
        button.setTitleColor(Colors.black, for: .normal)
        addSubview(button)
        
        heroView = UILabel()
        heroView.heroID = "heroTest"
        heroView.backgroundColor = UIColor.blue
        heroView.text = "Hero test"
        heroView.textAlignment = .center
        addSubview(heroView)
        
        constrain(button, heroView) { button, heroView in
            button.center == button.superview!.center
            
            heroView.centerX == heroView.superview!.centerX
            heroView.bottom == heroView.superview!.bottom - 10
            heroView.width == 150
            heroView.height == 100
        }
    }
}
