//
//  HeightViewModel.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol HeightViewModelProtocol: BaseViewModelProtocol {
    func next(height: Int)
    
    var busyChanged: ((Bool) -> Void)? { get set }
    var itemsChanged: (([SelectItem]) -> Void)? { get set }
}

class HeightViewModel: BaseViewModel, HeightViewModelProtocol {
    var userFacade: UserFacadeProtocol!
    
    var busyChanged: ((Bool) -> Void)?
    var itemsChanged: (([SelectItem]) -> Void)?
    var done: ((Router) -> Void)?
    
    init(done: ((Router) -> Void)?) {
        self.done = done
    }
    
    override func willAppear() {
        
        itemsChanged?((0...20).map({ 130 + $0*5 }).map({ SelectItem(id: "\($0)", value: $0) }))
    }
    
    private func raiseDone() {
        done?(router)
    }
    
    func next(height: Int) {
        busyChanged?(true)
        userFacade.getUser { [weak self] user in
            if let user = user {
                user.height = height
                self?.userFacade.update(user: user) { success in
                    self?.busyChanged?(false)
                    if success {
                        self?.raiseDone()
                    }
                }
            } else {
                self?.busyChanged?(false)
            }
        }
    }
}
