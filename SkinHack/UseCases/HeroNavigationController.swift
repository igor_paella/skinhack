//
//  HeroNavigationController.swift
//  SkinHack
//
//  Created by Игорь on 21/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class HeroNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isHeroEnabled = true
        heroNavigationAnimationType = .fade
    }
}
