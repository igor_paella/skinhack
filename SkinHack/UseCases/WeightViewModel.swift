//
//  WeightViewModel.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol WeightViewModelProtocol: BaseViewModelProtocol {
    func next(weight: Int)
    
    var busyChanged: ((Bool) -> Void)? { get set }
    var itemsChanged: (([SelectItem]) -> Void)? { get set }
}

class WeightViewModel: BaseViewModel, WeightViewModelProtocol {
    var userFacade: UserFacadeProtocol!
    
    var busyChanged: ((Bool) -> Void)?
    var itemsChanged: (([SelectItem]) -> Void)?
    var done: ((Router) -> Void)?
    
    init(done: ((Router) -> Void)?) {
        self.done = done
    }
    
    override func willAppear() {
        
        itemsChanged?((0...24).map({ 30 + $0*5 }).map({ SelectItem(id: "\($0)", value: $0) }))
    }
    
    private func raiseDone() {
        done?(router)
    }
    
    func next(weight: Int) {
        busyChanged?(true)
        userFacade.getUser { [weak self] user in
            if let user = user {
                user.weight = weight
                self?.userFacade.update(user: user) { success in
                    self?.busyChanged?(false)
                    if success {
                        self?.raiseDone()
                    }
                }
            } else {
                self?.busyChanged?(false)
            }
        }
    }
}
