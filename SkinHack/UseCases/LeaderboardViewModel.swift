//
//  LeaderboardViewModel.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol LeaderboardViewModelProtocol: CollectionViewModelProtocol {
    var isModal: Bool { get }
}

class LeaderboardViewModel: BaseCollectionViewModel, LeaderboardViewModelProtocol {
    var userFacade: UserFacadeProtocol!
    
    private var items: [LeaderboardItem] = []
    var isModal: Bool
    
    init(isModal: Bool) {
        self.isModal = isModal
    }
    
    override func willAppear() {
//        addFakeWorkouts()
        load()
    }
    
    func load() {
        userFacade.getLeaderboards { [weak self] items in
            self?.items = items
            self?.update()
        }
    }
    
    override func update() {
        var newItems: [BaseCellViewModel] = []
        let userId = userFacade.userId
        for (index, item) in items.enumerated() {
            newItems.append(LeaderboardCellViewModel(item: item, order: index + 1, selected: item.userId == userId))
            newItems.append(SpaceCellViewModel(height: 2))
        }
        updated?([newItems], .firstLoad)
    }
    
    private func addFakeWorkouts() {
        userFacade.addWorkout(item: LeaderboardItem(id: UUID().uuidString, userId: "facebook_user_123", name: "Анна Рябчикова", avatar: "https://graph.facebook.com/100000112282752/picture?type=large", res: Product.burger, result: 510.0, date: Date()), callback: { _ in })
        userFacade.addWorkout(item: LeaderboardItem(id: UUID().uuidString, userId: "facebook_user_124", name: "Irina Trushkina", avatar: "https://graph.facebook.com/711128692/picture?type=large", res: Product.banan, result: 100.0, date: Date()), callback: { _ in })
        userFacade.addWorkout(item: LeaderboardItem(id: UUID().uuidString, userId: "facebook_user_125", name: "Vyacheslav Dubovitsky", avatar: "https://graph.facebook.com/1498824471/picture?type=large", res: Product.burgerWithFries, result: 740.0, date: Date()), callback: { _ in })
        userFacade.addWorkout(item: LeaderboardItem(id: UUID().uuidString, userId: "facebook_user_126", name: "Andrei Burton", avatar: "https://graph.facebook.com/696276194/picture?type=large", res: Product.apple, result: 50.0, date: Date()), callback: { _ in })
        userFacade.addWorkout(item: LeaderboardItem(id: UUID().uuidString, userId: "facebook_user_127", name: "Platov Alex", avatar: "https://graph.facebook.com/100001702296842/picture?type=large", res: Product.banan, result: 110.0, date: Date()), callback: { _ in })
        userFacade.addWorkout(item: LeaderboardItem(id: UUID().uuidString, userId: "facebook_user_128", name: "Максим Денисов", avatar: "https://graph.facebook.com/100000226151098/picture?type=large", res: Product.carrot, result: 35.0, date: Date()), callback: { _ in })
        userFacade.addWorkout(item: LeaderboardItem(id: UUID().uuidString, userId: "facebook_user_129", name: "Антон Гладкобородов", avatar: "https://graph.facebook.com/1165382856/picture?type=large", res: Product.burgerWithFriesAndCoke, result: 900.0, date: Date()), callback: { _ in })
        
        userFacade.getUser { user in
            if let user = user {
                for i in 0...90 {
                    if self.randomNumber(inRange: 0...2) != 0 {
                        let res = self.randomNumber(inRange: 50...1000)
                        let prod = Product(calories: Float(res))
                        let date = Date().addingTimeInterval(TimeInterval(-1*i*24*60*60))
                        let item = LeaderboardItem(id: UUID().uuidString, userId: user.id, name: user.name, avatar: user.avatar, res: prod, result: Double(res), date: date)
                        self.userFacade.addWorkout(item: item, callback: { _ in })
                    }
                }
            }
        }
    }
    
    public func randomNumber<T: SignedInteger>(inRange range: ClosedRange<T> = 1...6) -> T {
        let length = Int64(range.upperBound - range.lowerBound + 1)
        let value = Int64(arc4random()) % length + Int64(range.lowerBound)
        return T(value)
    }
}
