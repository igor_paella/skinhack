//
//  RootViewController.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class RootViewController: ViewControllerBase, ViewModelHolder {
    
    typealias TViewModel = RootViewModelProtocol
    
    override func loadView() {
        super.loadView()

        Router.rootInit(vc: self)
    }    
}
