//
//  BaseCollectionLayout.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class BaseCollectionLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        
        scrollDirection = .vertical
        sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate var insertedIndexPaths = [IndexPath]()
    fileprivate var deletedIndexPaths = [IndexPath]()
    
    override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        super.prepare(forCollectionViewUpdates: updateItems)
        
        for updateItem in updateItems {
            if updateItem.updateAction == .insert {
                if let indexPathAfterUpdate = updateItem.indexPathAfterUpdate {
                    insertedIndexPaths.append(indexPathAfterUpdate)
                }
            } else if updateItem.updateAction == .delete {
                if let indexPathBeforeUpdate = updateItem.indexPathBeforeUpdate {
                    deletedIndexPaths.append(indexPathBeforeUpdate)
                }
            }
        }
    }
    
    override func finalizeCollectionViewUpdates() {
        super.finalizeCollectionViewUpdates()
        
        insertedIndexPaths.removeAll()
        deletedIndexPaths.removeAll()
    }
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes {
        if let attributes = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath) {
            if insertedIndexPaths.filter({ $0 == itemIndexPath }).count > 0 {
                attributes.alpha = 1
            }
            return attributes
        }
        return UICollectionViewLayoutAttributes(forCellWith: itemIndexPath)
    }
    
    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes {
        if let attributes = super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath) {
            
            if deletedIndexPaths.filter({ $0 == itemIndexPath }).count > 0 {
                attributes.alpha = 0
            }
            return attributes
        }
        return UICollectionViewLayoutAttributes(forCellWith: itemIndexPath)
    }
}
