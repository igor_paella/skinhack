//
//  BaseViewController.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class BaseViewController<T: BaseView>: ViewControllerBase {
    
    internal var baseView: T!
    internal var topConstraint: NSLayoutConstraint!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func setupView() {
        super.setupView()
        
        baseView = T()
        
        view.addSubview(baseView)
        constrain(baseView) { baseView in
            baseView.height == baseView.superview!.height
            baseView.width == baseView.superview!.width
            baseView.centerX == baseView.superview!.centerX
            topConstraint = baseView.top == baseView.superview!.top
        }
    }
}
