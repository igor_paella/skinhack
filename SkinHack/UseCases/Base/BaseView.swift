//
//  BaseView.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class BaseView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    internal func setupView() {
        
    }
}
