//
//  ViewControllerBase.swift
//  SkinHack
//
//  Created by Игорь on 22/05/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class ViewControllerBase: UIViewController {
    
    var vm: BaseViewModelProtocol!
    
    func closeModal() {
        dismiss(animated: true, completion: nil)
    }
    
    func pop() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func popClose() {
        if let nav = navigationController,
            nav.viewControllers.count > 1 {
            _ = nav.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        vm.didLoad()
    }
    
    internal func setupView() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        vm.willAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        vm.didAppear()
    }
}
