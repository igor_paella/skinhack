//
//  CollectionViewModelProtocol.swift
//  SkinHack
//
//  Created by Игорь on 02/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol CollectionViewModelProtocol: BaseViewModelProtocol {
    var updated: (([[BaseCellViewModel]], _ context: UpdateContext) -> Void)? { get set }
    var changed: ((BaseCellViewModel) -> Void)? { get set }
    var deleted: ((BaseCellViewModel) -> Void)? { get set }
    var heightChanged: ((BaseCellViewModel) -> Void)? { get set }
    
    func update()
    func touched(item: BaseCellViewModel, indexPath: IndexPath)
}
