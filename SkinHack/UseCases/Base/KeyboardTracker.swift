//
//  KeyboardTracker.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

// swiftlint:disable force_cast
// swiftlint:disable block_based_kvo

import Foundation

class AnimationParams {
    var curve: UIViewAnimationCurve
    var duration: TimeInterval
    
    init(curve: UIViewAnimationCurve, duration: TimeInterval) {
        self.curve = curve
        self.duration = duration
    }
}

class DummyAccessoryView: UIView {
    
    var frameChanged: (() -> Void)?
    fileprivate var observedView: UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    fileprivate func commonInit() {
        self.autoresizingMask = .flexibleHeight
        //        self.userInteractionEnabled = false
        self.backgroundColor = UIColor.clear
        self.isHidden = true
    }
    
    deinit {
        if let observedView = self.observedView {
            observedView.removeObserver(self, forKeyPath: "center")
        }
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        if let observedView = self.observedView {
            observedView.removeObserver(self, forKeyPath: "center")
            self.observedView = nil
        }
        
        if let newSuperview = newSuperview {
            newSuperview.addObserver(self, forKeyPath: "center", options: [.new, .old], context: nil)
            self.observedView = newSuperview
        }
        
        super.willMove(toSuperview: newSuperview)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if /*object === superview &&*/ keyPath == "center" {
            guard let change = change else { return }
            let oldCenter = (change[NSKeyValueChangeKey.oldKey] as! NSValue).cgPointValue
            let newCenter = (change[NSKeyValueChangeKey.newKey] as! NSValue).cgPointValue
            if oldCenter != newCenter {
                frameChanged?()
            }
        }
    }
}

class KeyboardTracker: NSObject {
    
    static var keyboardHeight: CGFloat = 0
    fileprivate var view: UIView
    fileprivate var inputView: DummyAccessoryView
    var keyboardChanged: ((CGFloat, AnimationParams?) -> Void)?
    
    init(view: UIView, inputView: DummyAccessoryView) {
        self.view = view
        self.inputView = inputView
        
        super.init()
        
        self.inputView.frameChanged = { [weak self] in
            self?.inputAccessoryViewFrameChanged()
        }
    }
    
    func startTracking() {
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardTracker.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardTracker.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardTracker.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardTracker.keyboardWillChangeFrame(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func stopTracking() {
        NotificationCenter.default.removeObserver(self) // swiftlint:disable:this notification_center_detachment
    }
    
    fileprivate func inputAccessoryViewFrameChanged() {
        if let keyboardFrame = inputView.superview?.frame {
            let bottomConstraint = bottomConstraintForRect(keyboardFrame)
            keyboardChanged?(bottomConstraint, nil)
        }
    }
    
    @objc
    fileprivate func keyboardWillShow(_ notification: Notification) {
        let bottomConstraint = self.bottomConstraintFromNotification(notification)
        guard bottomConstraint > 0 else { return } // Some keyboards may report initial willShow/DidShow notifications with invalid positions
        keyboardChanged?(bottomConstraint, getAnimationParams(notification))
    }
    
    @objc
    fileprivate func keyboardDidShow(_ notification: Notification) {
        let bottomConstraint = self.bottomConstraintFromNotification(notification)
        guard bottomConstraint > 0 else { return } // Some keyboards may report initial willShow/DidShow notifications with invalid positions
        keyboardChanged?(bottomConstraint, getAnimationParams(notification))
    }
    
    @objc
    fileprivate func keyboardWillChangeFrame(_ notification: Notification) {
        let bottomConstraint = self.bottomConstraintFromNotification(notification)
        keyboardChanged?(bottomConstraint, getAnimationParams(notification))
    }
    
    @objc
    fileprivate func keyboardWillHide(_ notification: Notification) {
        keyboardChanged?(0, getAnimationParams(notification))
    }
    
    fileprivate func getAnimationParams(_ notification: Notification) -> AnimationParams? {
        if let userInfo = (notification as NSNotification).userInfo {
            let animationCurve = UIViewAnimationCurve(rawValue: (userInfo[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).intValue)!
            let animationDuration = TimeInterval((userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).intValue)
            return AnimationParams(curve: animationCurve, duration: animationDuration)
        }
        return nil
    }
    
    fileprivate func bottomConstraintFromNotification(_ notification: Notification) -> CGFloat {
        guard let rect = ((notification as NSNotification).userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return 0 }
        guard rect.height > 0 else { return 0 }
        KeyboardTracker.keyboardHeight = rect.height
        let rectInView = self.view.convert(rect, from: nil)
        return bottomConstraintForRect(rectInView)
    }
    fileprivate func bottomConstraintForRect(_ rect: CGRect) -> CGFloat {
        guard rect.maxY >= self.view.bounds.height else { return 0 } // Undocked keyboard
        return max(0, self.view.bounds.height - rect.minY)//- self.trackingView.bounds.height)
    }
}
