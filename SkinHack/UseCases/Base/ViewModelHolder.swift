//
//  ViewModelHolder.swift
//  SkinHack
//
//  Created by Игорь on 22/05/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

// swiftlint:disable force_cast

import Foundation

protocol ViewModelHolder {
    associatedtype TViewModel
    
    var vm: BaseViewModelProtocol! { get set }
    var viewModel: TViewModel { get set }
}

extension ViewModelHolder {
    var viewModel: TViewModel {
        get {
            return vm as! TViewModel
        }
        set {
            vm = newValue as! BaseViewModelProtocol
        }
    }
}
