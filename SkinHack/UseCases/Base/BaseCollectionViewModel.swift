//
//  BaseCollectionViewModel.swift
//  SkinHack
//
//  Created by Игорь on 02/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class BaseCollectionViewModel: BaseViewModel, CollectionViewModelProtocol {
    var updated: (([[BaseCellViewModel]], _ context: UpdateContext) -> Void)?
    var changed: ((BaseCellViewModel) -> Void)?
    var deleted: ((BaseCellViewModel) -> Void)?
    var heightChanged: ((BaseCellViewModel) -> Void)?
    
    func update() {
    }
    
    func touched(item: BaseCellViewModel, indexPath: IndexPath) {
    }
}
