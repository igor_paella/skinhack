//
//  BaseViewModel.swift
//  SkinHack
//
//  Created by Игорь on 02/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class BaseViewModel: BaseViewModelProtocol {
    let router = Router()
    
    func didLoad() {
        
    }
    
    func willAppear() {
        
    }
    
    func didAppear() {
        
    }
}
