//
//  BaseViewModelProtocol.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol BaseViewModelProtocol: class {
    
    func didLoad()
    func didAppear()
    func willAppear()
    
    var router: Router { get }
}
