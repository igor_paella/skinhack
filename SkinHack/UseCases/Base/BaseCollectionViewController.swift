//
//  BaseCollectionViewController.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

// swiftlint:disable force_cast
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable file_length

import Foundation
import Cartography

public enum UpdateContext {
    case normal
    case firstLoad
    case pagination
    case reload
    case syncLoad
}

// subclass must implement ViewModelHolder protocol
// with TViewModel type inherited from CollectionViewModelProtocol

class BaseCollectionViewController: ViewControllerBase, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private var collectionVM: CollectionViewModelProtocol {
        return vm as! CollectionViewModelProtocol
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    internal var needGradient: Bool {
        return false
    }
    
    internal var collectionViewToBottom: CGFloat {
        return 0
    }
    
    internal var container: UIView!
    internal var collectionViewContainer: UIView!
    internal var collectionView: UICollectionView!
    internal var collectionViewLayout: UICollectionViewLayout!
    internal var items = [[BaseCellViewModel]]()
    internal var accessoryView: DummyAccessoryView!
    
    private var registeredClass: [String: CollectionBaseCell.Type] = [:]
    private let updateQueue = DispatchQueue(label: "com.updateQueue", attributes: [])
    private let updateSemaphore = DispatchSemaphore(value: 1)
    private var _emptyView: UIView?
    
    internal func createEmptyView() -> UIView? {
        return nil
    }
    
    internal func createAccessoryView() -> DummyAccessoryView {
        return DummyAccessoryView()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.collectionViewLayout = BaseCollectionLayout()
    }
    
    init(collectionViewLayout: UICollectionViewLayout) {
        super.init(nibName: nil, bundle: nil)
        
        self.collectionViewLayout = collectionViewLayout
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var keyboardTracker: KeyboardTracker!
    
    internal var cellType: CellsRegistrotor {
        fatalError("need override cellType in subclass")
    }
    
    internal var topOffset: CGFloat {
        return (navigationController?.navigationBar.frame.height ?? 0) + UIApplication.shared.statusBarFrame.size.height
    }
    
    override var inputAccessoryView: UIView {
        return accessoryView
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    // MARK: - lifecycle
    
    override func setupView() {
        super.setupView()
        
        view.backgroundColor = Colors.white
        //        edgesForExtendedLayout = UIRectEdge.None
        //        automaticallyAdjustsScrollViewInsets = false
        container = UIView()
        view.addSubview(container)
        
        collectionViewContainer = UIView()
        container.addSubview(collectionViewContainer)
        
        collectionView = UICollectionView(frame: view.frame, collectionViewLayout: collectionViewLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionViewContainer.addSubview(collectionView)
        
        constrain(container) { container in
            container.top == container.superview!.top
            container.size == container.superview!.size
            container.centerX == container.superview!.centerX ~ LayoutPriority(rawValue: ConstraintPriority.medium.rawValue)
        }
        
        addCollectionViewConstraints()
        
        constrain(collectionView) { collectionView in
            collectionView.top == collectionView.superview!.top
            collectionView.bottom == collectionView.superview!.bottom
            collectionView.left == collectionView.superview!.left
            collectionView.right == collectionView.superview!.right
        }
        accessoryView = createAccessoryView()
        _emptyView = createEmptyView()
        if let emptyView = _emptyView {
            emptyView.isHidden = true
            collectionView.addSubview(emptyView)
            constrain(emptyView) { emptyView in
                emptyView.centerX == emptyView.superview!.centerX
                emptyView.centerY == emptyView.superview!.centerY - 35
            }
        }
        
        if needGradient {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [Colors.black.cgColor, Colors.black.cgColor, Colors.black.withAlphaComponent(0.4).cgColor, Colors.clear.cgColor]
            gradientLayer.locations = [0.0, 0.96, 0.98, 1.0]
            gradientLayer.frame = CGRect(x: 0, y: 0, width: collectionView.frame.width, height: collectionView.frame.height)
            collectionViewContainer.layer.mask = gradientLayer
        }
        
        cellType.registerCells()
        navigationController?.navigationBar.topItem?.title = ""
        collectionView.alwaysBounceVertical = true
        collectionView.keyboardDismissMode = .interactive
        collectionView.backgroundColor = Colors.clear
        
        keyboardTracker = KeyboardTracker(view: view, inputView: accessoryView)
        keyboardTracker.keyboardChanged = { [weak self] bottomValue, params in
            self?.keyboardFrameChanged(bottomValue, animationParams: params)
        }

        weak var weakSelf = self
        collectionVM.updated = { weakSelf?.updateItems($0, context: $1) }
        collectionVM.changed = { weakSelf?.reloadCellForItemId($0.id) }
        collectionVM.deleted = { weakSelf?.deleteCellForItemId($0.id) }
        collectionVM.heightChanged = { weakSelf?.cellHeightDidChange($0.id) }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        collectionViewContainer.layer.mask?.frame = CGRect(x: 0, y: 0, width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    internal func addCollectionViewConstraints() {
        constrain(collectionViewContainer) { collectionViewContainer in
            
            collectionViewContainer.top == collectionViewContainer.superview!.top
            collectionViewContainer.bottom == collectionViewContainer.superview!.bottom - collectionViewToBottom
            collectionViewContainer.left == collectionViewContainer.superview!.left
            collectionViewContainer.right == collectionViewContainer.superview!.right
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        keyboardTracker.startTracking()
        NotificationCenter.default.addObserver(self, selector: #selector(BaseCollectionViewController.willEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BaseCollectionViewController.didEnterBackground(_:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        keyboardTracker.stopTracking()
        NotificationCenter.default.removeObserver(self) // swiftlint:disable:this notification_center_detachment
    }
    
    func registerClass(_ cell: CollectionBaseCell.Type, identifier: String) {
        registeredClass[identifier] = cell
        collectionView.register(cell, forCellWithReuseIdentifier: identifier)
    }
    
    func classForIdentifier(_ identifier: String) -> CollectionBaseCell.Type {
        return registeredClass[identifier]!
    }
    
    internal func updateItemsCompleted() {
        
    }
    
    internal func updateItems(_ newItems: [[BaseCellViewModel]], context: UpdateContext, callback: (() -> Void)? = nil) {
        updateModels(newItems, context: context) { [weak self] in
            self?.updateItemsCompleted()
            callback?()
        }
    }
    
    internal func scrollToBottom(animated: Bool) {
        let offsetY = bottomOffsetY()
        collectionView!.setContentOffset(CGPoint(x: 0, y: offsetY), animated: animated)
    }
    
    internal func bottomOffsetY() -> CGFloat {
        return max(-collectionView!.contentInset.top, collectionView!.collectionViewLayout.collectionViewContentSize.height - collectionView!.bounds.height + collectionView!.contentInset.bottom)
    }
    
    internal func isBottomCloser(offsetY: CGFloat = 0) -> Bool {
        let bottomY = bottomOffsetY()
        let diff = abs(collectionView!.contentOffset.y + offsetY - bottomY)
        return diff < 10
    }
    
    // MARK: - Items changes
    
    private func updateModels(_ newItems: [[BaseCellViewModel]], context: UpdateContext, callback: @escaping () -> Void) {
        processItemsChanges(newItems, context: context, callback: { [weak self] (changes, newItems, completion: @escaping (() -> Void)) in
            if let strongSelf = self {
                let compl = {
                    completion()
                }
                strongSelf.updateItemsProcess(newItems, changes: changes, context: context) { [weak self] in
                    self?.checkShowEmpty()
                    callback()
                    compl()
                }
            }
        })
    }
    
    internal func showEmptyCondition() -> Bool {
        return items.filter({ $0.filter({ !($0 is SpaceCellViewModel) }).count > 0 }).count == 0
    }
    
    internal func configureEmptyBeforeShow() {
        
    }
    
    private func checkShowEmpty() {
        let show = showEmptyCondition()
        if show {
            configureEmptyBeforeShow()
        }
        _emptyView?.isHidden = !show
    }
    
    private func processItemsChanges(_ newItems: [[BaseCellViewModel]], context: UpdateContext, callback: @escaping (_: CollectionChanges, _: [[BaseCellViewModel]], _: @escaping () -> Void) -> Void) {
        let collectionViewWidth = getCollectionWidth()
        if context == .syncLoad {
            let changes = generateChangesFor(items: newItems, context: context, width: collectionViewWidth)
            callback(changes, newItems, {})
        } else {
            updateQueue.async { [weak self] in
                guard let strongSelf = self else { return }
                _ = strongSelf.updateSemaphore.wait(timeout: DispatchTime.distantFuture)
                let changes = strongSelf.generateChangesFor(items: newItems, context: context, width: collectionViewWidth)
                DispatchQueue.main.async {
                    callback(changes, newItems, {
                        strongSelf.updateSemaphore.signal()
                    })
                }
            }
        }
    }
    
    private func generateChangesFor(items newItems: [[BaseCellViewModel]], context: UpdateContext, width: CGFloat) -> CollectionChanges {
        for section in newItems {
            for item in section {
                processVMLayout(item: item, width: width)
            }
        }
        let changes = context == .normal ? generateChanges(oldCollection: items, newCollection: newItems) : CollectionChanges.empty
        return changes
    }
    
    private func getCollectionWidth() -> CGFloat {
        let inset = (collectionViewLayout as? UICollectionViewFlowLayout)?.sectionInset ?? UIEdgeInsets.zero
        return collectionView!.bounds.width - inset.left - inset.right
    }
    
    private func processVMLayout(item: BaseCellViewModel, width: CGFloat) {
        if let autolayoutVM = item as? BaseAutolayoutCellViewModel {
            if autolayoutVM.canProcessInBackground {
                autolayoutVM.processInBackground(width)
            } else {
                DispatchQueue.main.sync {
                    item.processLayout(width, collectionView: self)
                }
            }
        } else {
            item.processLayout(width, collectionView: self)
        }
    }
    
    private func needScrollToBottom(_ context: UpdateContext) -> Bool {
        switch context {
        case .pagination:
            return false
        default:
            return false
        }
    }
    
    private func deleteCellForItemId(_ itemId: String) {
        if let indexPath = indexPathForItemId(itemId) {
            items[indexPath.section].remove(at: indexPath.row)
            collectionView.deleteItems(at: [indexPath])
        }
    }
    
    internal func reloadCellForItemId(_ itemId: String) {
        if let indexPath = indexPathForItemId(itemId) {
            collectionView.reloadItems(at: [indexPath])
        }
    }
    
    internal func reloadCellsForItemIds(_ itemIds: [String]) {
        var indexPaths = [IndexPath]()
        itemIds.forEach({
            if let indexPath = indexPathForItemId($0), !indexPaths.contains(indexPath) {
                indexPaths.append(indexPath)
            }
        })
        collectionView.reloadItems(at: indexPaths)
    }
    
    internal func cellForItemId(_ itemId: String) -> CollectionBaseCell? {
        if let indexPath = indexPathForItemId(itemId) {
            collectionView.layoutIfNeeded()
            return collectionView.cellForItem(at: indexPath) as? CollectionBaseCell
        }
        return nil
    }
    
    func updateItemsProcess(_ newItems: [[BaseCellViewModel]], changes: CollectionChanges, context: UpdateContext, callback: @escaping (() -> Void)) {
        
        let shouldScrollToBottom = needScrollToBottom(context)
        
        let visibleIndexPath = collectionView.indexPathsForVisibleItems.sorted(by: { $0 < $1 })
        let oldIndexPath = visibleIndexPath.first
        let oldRect = self.rectAtIndexPath(oldIndexPath)
        let oldItemId: String? = oldIndexPath == nil ? nil : items[oldIndexPath!.section][oldIndexPath!.row].id
        let oldContentOffsetY = collectionView.contentOffset.y
        
        let completion = {
            DispatchQueue.main.async {
                callback()
            }
        }
        self.items = newItems
        
        switch context {
        case .normal:
            UIView.animate(withDuration: 0.3, animations: {
                self.updateVisibleCells(changes)
                self.collectionView.performBatchUpdates({
                    self.collectionView!.deleteItems(at: Array(changes.deletedIndexPaths) as [IndexPath])
                    self.collectionView!.insertItems(at: Array(changes.insertedIndexPaths) as [IndexPath])
                    for move in changes.movedIndexPaths {
                        self.collectionView!.moveItem(at: move.indexPathOld as IndexPath, to: move.indexPathNew as IndexPath)
                    }
                    self.collectionView!.insertSections(changes.insertedSections)
                    self.collectionView!.deleteSections(changes.deletedSections)
                }, completion: { _ -> Void in
                    completion()
                })
            })
        default:
            collectionView.reloadData()
            collectionView.collectionViewLayout.prepare()
            callback()
        }
        
        if shouldScrollToBottom {
            scrollToBottom(animated: context == .normal)
        } else {
            switch context {
            case .firstLoad, .syncLoad:
                break
            default:
                if let newOldIndexPath = indexPathForItemId(oldItemId) {
                    let newRect = self.rectAtIndexPath(newOldIndexPath)
                    self.scrollToPreservePosition(oldRefRect: oldRect, newRefRect: newRect, oldContentOffsetY: oldContentOffsetY)
                }
            }
        }
    }
    private func indexPathForItemId(_ itemId: String?) -> IndexPath? {
        for (section, sectionItems) in items.enumerated() {
            for (row, item) in sectionItems.enumerated() where item.id == itemId {
                return IndexPath(item: row, section: section)
            }
        }
        return nil
    }
    private func rectAtIndexPath(_ indexPath: IndexPath?) -> CGRect? {
        if let indexPath = indexPath {
            return self.collectionView.collectionViewLayout.layoutAttributesForItem(at: indexPath)?.frame
        }
        return nil
    }
    private func scrollToPreservePosition(oldRefRect: CGRect?, newRefRect: CGRect?, oldContentOffsetY: CGFloat) {
        guard let collectionView = collectionView, let oldRefRect = oldRefRect, let newRefRect = newRefRect else {
            return
        }
        let diffY = newRefRect.minY - oldRefRect.minY
        collectionView.contentOffset = CGPoint(x: 0, y: oldContentOffsetY + diffY)
    }
    
    internal func updateHeightForItemWithId(_ vmId: String) {
        if let indexPath = indexPathForItemId(vmId) {
            let vm = items[indexPath.section][indexPath.row]
            if let cell = collectionView.cellForItem(at: indexPath) as? CollectionBaseCell {
                collectionView.collectionViewLayout.invalidateLayout()
                let newHeight = cell.contentView.systemLayoutSizeFitting(CGSize(width: cell.frame.width, height: CGFloat.greatestFiniteMagnitude)).height
                var newFrame = cell.frame
                newFrame.size.height = newHeight
                collectionView.performBatchUpdates({
                    vm.cellLayout.frame.size = newFrame.size
                    cell.frame = newFrame
                }, completion: nil)
            } else {
                let width = getCollectionWidth()
                updateQueue.async { [weak self] in
                    self?.processVMLayout(item: vm, width: width)
                    DispatchQueue.main.sync {
                        self?.collectionView.collectionViewLayout.invalidateLayout()
                    }
                }
            }
        }
    }
    
    func updateVisibleCells(_ changes: CollectionChanges) {
        let visibleIndexPaths = Set(self.collectionView!.indexPathsForVisibleItems.filter { (indexPath) -> Bool in
            return !changes.insertedIndexPaths.contains(indexPath) && !changes.deletedIndexPaths.contains(indexPath)
        })
        
        var updatedIndexPaths = Set<IndexPath>()
        
        for move in changes.movedIndexPaths {
            updatedIndexPaths.insert(move.indexPathOld as IndexPath)
            if let cell = self.collectionView!.cellForItem(at: move.indexPathOld as IndexPath) as? CollectionBaseCell {
                let item = items[move.indexPathNew.section][move.indexPathNew.row]
                cell.configurate(item, visible: true, prototype: false)
            }
        }
        
        // Update remaining visible cells
        let remaining = visibleIndexPaths.subtracting(updatedIndexPaths)
        for indexPath in remaining {
            if let cell = self.collectionView!.cellForItem(at: indexPath) as? CollectionBaseCell {
                if indexPath.section < items.count && indexPath.row < items[indexPath.section].count {
                    let item = items[indexPath.section][indexPath.row]
                    cell.configurate(item, visible: true, prototype: false)
                }
            }
        }
    }
    
    internal func getViewModelById<T>(_: T.Type, _ id: String) -> (IndexPath, T)? {
        for (section, sectionItems) in items.enumerated() {
            for (row, item) in sectionItems.enumerated() where item.id == id {
                return (IndexPath(item: row, section: section), item as! T)
            }
        }
        return nil
    }
    
    // MARK: - keyboard interact
    
    internal func keyboardFrameChanged(_ bottomValue: CGFloat, animationParams: AnimationParams?) {
        
    }
    
    @objc internal func willEnterForeground(_ notification: Notification) {
        
    }
    
    @objc internal func didEnterBackground(_ notification: Notification) {
        
    }
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.section][indexPath.row]
        let cell = dequeueReusableCell(item.identifier, indexPath: indexPath)
        cell.configurate(item, visible: false, prototype: false)
        return cell
    }
    
    private func dequeueReusableCell(_ identifier: String, indexPath: IndexPath) -> CollectionBaseCell {
        return collectionView!.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CollectionBaseCell
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as! CollectionBaseCell).willDisplay()
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as! CollectionBaseCell).didEndDisplay()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        collectionVM.touched(item: items[indexPath.section][indexPath.row], indexPath: indexPath)
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = items[indexPath.section][indexPath.row]
        return item.cellLayout.frame.size
    }
}

extension BaseCollectionViewController: CellHeightDelegate {
    
    func cellHeightDidChange(_ vmId: String) {
        updateHeightForItemWithId(vmId)
    }
}
