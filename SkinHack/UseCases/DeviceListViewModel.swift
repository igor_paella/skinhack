//
//  DeviceListViewModel.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import MetaWear

protocol DeviceListViewModelProtocol: CollectionViewModelProtocol {
    func reload()
}

class DeviceListViewModel: BaseCollectionViewModel, DeviceListViewModelProtocol {
    var tshirtFacade: TShirtFacadeProtocol!
    
    private var devices: [DeviceName] = []
    private var savedShirtTimer: Timer?
    
    override func willAppear() {
        reload()
    }
    
    func reload() {
        Activity.instance.busy(true)
        savedShirtTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { [weak self] _ in
            self?.tshirtFacade.disconnect(completed: {
                self?.findDevices()
            })
        })
        
        tshirtFacade.checkConnected(completed: { [weak self] success in
            if success {
                Activity.instance.busy(false)
                self?.tshirtFacade.flash()
                self?.router.start().push()
                //self?.checkConnection()

            } else {
                self?.findDevices()
            }
        })
    }
    
    func findDevices() {
        savedShirtTimer?.invalidate()
        
        tshirtFacade.findDevices(completed: { [weak self] devices in
            Activity.instance.busy(false)
            self?.devices = devices
            self?.update()
        })
    }
    
    override func update() {
        var newItems: [BaseCellViewModel] = []
        for device in devices {
            newItems.append(TextCellViewModel(text: NSAttributedString(string: device.name, attributes: [
                NSAttributedStringKey.foregroundColor: Colors.white.withAlphaComponent(0.54),
                NSAttributedStringKey.font: Fonts.Display.medium(20)
                ]), background: UIColor(netHex: 0xD8D8D8).withAlphaComponent(0.1), height: 60, tag: device.id))
            newItems.append(SpaceCellViewModel(height: 2))
        }
        updated?([newItems], .firstLoad)
    }
    
    override func touched(item: BaseCellViewModel, indexPath: IndexPath) {
        guard let textVM = item as? TextCellViewModel,
            let deviceId = textVM.tag else { return }
        
        Activity.instance.busy(true)
        tshirtFacade.connect(deviceById: deviceId, callback: { [weak self] success in
            Activity.instance.busy(false)
            if success {
                self?.checkConnection()
                //self?.router.start().push()
            } else {
                self?.router.message(title: "Error", message: "Не удалось подключиться")
            }
        })
    }
    
    func checkConnection() {
        tshirtFacade.flash()
        router.alert(message: "Led blinking on \(tshirtFacade.currentDevice?.name ?? "")?", okay: { [weak self] in
            self?.router.start().push()
            self?.tshirtFacade.stopFlashing()
        }, cancel: { [weak self] in
            self?.tshirtFacade.stopFlashing()
            self?.tshirtFacade.disconnect(completed: {
                self?.reload()
            })
        })
    }
}
