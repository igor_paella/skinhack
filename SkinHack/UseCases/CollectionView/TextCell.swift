//
//  TextCell.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class TextCell: BaseCollectionCell<TextCellViewModel, AutolayoutCellLayout> {
    
    var labelView: UILabel!
    fileprivate var topConstraint: NSLayoutConstraint!
    fileprivate var bottomConstraint: NSLayoutConstraint!
    fileprivate var leftConstraint: NSLayoutConstraint!
    fileprivate var rightConstraint: NSLayoutConstraint!
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = Colors.clear
        labelView = UILabel()
        labelView.numberOfLines = 0
        contentView.addSubview(labelView)
        
        constrain(labelView) { label in
            
            topConstraint = label.top == label.superview!.top
            bottomConstraint = label.bottom == label.superview!.bottom
            leftConstraint = label.left == label.superview!.left + 16
            rightConstraint = label.right == label.superview!.right - 16
        }
    }
    
    override func configurate(_ item: TextCellViewModel, visible: Bool, prototype: Bool) {
        super.configurate(item, visible: visible, prototype: prototype)
        
        labelView.textAlignment = item.aligment
        labelView.attributedText = item.text
        topConstraint.constant = item.top
        bottomConstraint.constant = -item.bottom
        leftConstraint.constant = item.left
        rightConstraint.constant = -item.right
    }
}
