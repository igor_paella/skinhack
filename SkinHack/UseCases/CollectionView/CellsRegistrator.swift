//
//  CellsRegistrator.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class CellIdentifiers {
    class func identifierFor(_ vmType: AnyClass) -> String {
        return String(describing: type(of: vmType)).components(separatedBy: ".")[0]
    }
}

enum CellsRegistrotor {
    case test(BaseCollectionViewController)
    case lead(BaseCollectionViewController)
    case calendar(BaseCollectionViewController)
    
    func registerCells() {
        switch self {
        case .test(let collectionView):
            collectionView.regCollectionCells([
                (TextCell.self, TextCellViewModel.self),
                (SpaceCell.self, SpaceCellViewModel.self)
            ])
        case .lead(let collectionView):
            collectionView.regCollectionCells([
                (TextCell.self, TextCellViewModel.self),
                (SpaceCell.self, SpaceCellViewModel.self),
                (LeaderboardCell.self, LeaderboardCellViewModel.self)
            ])
        case .calendar(let collectionView):
            collectionView.regCollectionCells([
                (CalendarCell.self, CalendarCellViewModel.self),
                (TextCell.self, TextCellViewModel.self),
                (SpaceCell.self, SpaceCellViewModel.self),
                (GroupCell.self, GroupCellViewModel.self)
            ])
        }
    }
}

extension BaseCollectionViewController {
    
    internal func regCollectionCells(_ cells: [(CollectionBaseCell.Type, BaseCellViewModel.Type)]) {
        for cell in cells {
            self.registerClass(cell.0, identifier: CellIdentifiers.identifierFor(cell.1))
        }
    }
}
