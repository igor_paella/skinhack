//
//  TextCellViewModel.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class TextCellViewModel: BaseAutolayoutCellViewModel {
    
    var text: NSAttributedString!
    var aligment: NSTextAlignment!
    var top: CGFloat = 0
    var bottom: CGFloat = 0
    var left: CGFloat = 0
    var right: CGFloat = 0
    var height: CGFloat?
    var tag: UUID?
    
    init(text: NSAttributedString, aligment: NSTextAlignment = .left, background: UIColor = Colors.clear, top: CGFloat = 0, bottom: CGFloat = 0, left: CGFloat = 16, right: CGFloat = 16, height: CGFloat? = nil, id: String? = nil, tag: UUID? = nil) {
        super.init(id: id ?? text.string)
        
        self.text = text
        self.aligment = aligment
        self.background = background
        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right
        self.height = height
        self.tag = tag
    }
    
    override var cellHeight: CGFloat? {
        return height
    }
}
