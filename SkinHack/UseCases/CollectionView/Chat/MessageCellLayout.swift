//
//  MessageCellLayout.swift
//  SkinHack
//
//  Created by Игорь on 09/05/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class MessageCellLayout: BaseCellLayout {
    var avatarLayout: ImageLayout
    var bodyLayout: AttributedTextLayout

    init(width: CGFloat, avatarLayout: ImageLayout, bodyLayout: AttributedTextLayout, frame: CGRect) {
        
        self.avatarLayout = avatarLayout
        self.bodyLayout = bodyLayout
        
        super.init(width: width, frame: frame)
    }
    
//    init(baseLayout: MessageCellLayout) {
//        self.avatarLayout = baseLayout.avatarLayout
//        self.bodyLayout = baseLayout.bodyLayout
//
//        super.init(width: baseLayout.width, frame: baseLayout.frame)
//    }
}
