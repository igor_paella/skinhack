//
//  MessageCell.swift
//  SkinHack
//
//  Created by Игорь on 09/05/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

final class MessageTextView: UITextView {
    
    override var canBecomeFirstResponder: Bool {
        return false
    }
    
    override func addGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        if type(of: gestureRecognizer) == UILongPressGestureRecognizer.self && gestureRecognizer.delaysTouchesEnded {
            super.addGestureRecognizer(gestureRecognizer)
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}

class MessageCell: BaseCollectionCell<MessageCellViewModel, MessageCellLayout>, UITextViewDelegate {

    internal var body: MessageTextView!
    internal var avatar: UIImageView!
    
    override func setupViews() {
        super.setupViews()
        
        avatar = UIImageView()
        avatar.layer.cornerRadius = 7
        avatar.layer.masksToBounds = true
        avatar.isUserInteractionEnabled = true
        contentView.addSubview(avatar)
        
        body = MessageTextView()
        body.isEditable = false
        body.layoutManager.allowsNonContiguousLayout = true
        body.isScrollEnabled = false
        body.dataDetectorTypes = UIDataDetectorTypes()
        body.backgroundColor = UIColor.clear
        body.textContainerInset = UIEdgeInsets.zero
        body.textContainer.lineFragmentPadding = 0
        contentView.addSubview(body)
    }
    
    override func configurate(_ item: MessageCellViewModel, visible: Bool) {
        super.configurate(item, visible: visible)
        
        avatar.setImageByUrl(item.message.avatarUrl)
    }
    
    override func applyLayout(_ layout: MessageCellLayout, visible: Bool) {
        super.applyLayout(layout, visible: visible)
        
        avatar.applyLayout(layout.avatarLayout)
        body.applyLayout(layout.bodyLayout)
    }
    
    // MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange) -> Bool {
        return false
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return false
    }
}
