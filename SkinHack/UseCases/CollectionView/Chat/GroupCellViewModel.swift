//
//  GroupCellViewModel.swift
//  SkinHack
//
//  Created by Игорь on 09/05/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class Chat {
    var id: String = ""
    var text: String?
    var avatarUrl: String?
    
    init(id: String, text: String?, avatarUrl: String?) {
        self.id = id
        self.text = text
        self.avatarUrl = avatarUrl
    }
}

class GroupCellViewModel: BaseCellViewModel {
    
    var chat: Chat
    var attributedText: NSAttributedString
    
    init(chat: Chat) {
        self.chat = chat
        attributedText = NSAttributedString(string: chat.text ?? "", attributes: [NSAttributedStringKey.font: Fonts.Display.regular(16)])
        
        super.init(id: chat.id)
    }
    
    override func processLayout(_ width: CGFloat) {
        super.processLayout(width)
        
        let avatarLayout = ImageLayout(frame: CGRect(x: 10, y: 10, width: 20, height: 20), source: .network(chat.avatarUrl))
        let bodyLayout = AttributedTextLayout(frame: CGRect(x: 40, y: 10, width: width - 50, height: 20), text: attributedText)
        self.cellLayout = GroupCellLayout(width: width, avatarLayout: avatarLayout, bodyLayout: bodyLayout, frame: CGRect(x: 0, y: 0, width: width, height: 40))
    }
}
