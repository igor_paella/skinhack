//
//  BaseCellViewModel.swift
//  SkinHack
//
//  Created by Игорь on 20/12/15.
//  Copyright © 2016 ballr. All rights reserved.
//

import Foundation

class BaseCellViewModel {
    
    var background: UIColor?
    var cellLayout: BaseCellLayout!
    
    var id: String = ""
//    var oldId: String?
    
    var idInt: Int64 {
        return Int64(id) ?? 0
    }
    
    init(id: String) {
        self.id = id
    }
    
    init () {
        self.id = UUID().uuidString
    }
    
//    var identifier: String {
//        return BaseCellViewModel.ident(self.dynamicType)
//    }
//    
//    class func ident(type: BaseCellViewModel.Type) -> String {
//        return String(type)
//    }
    
    var identifier: String {
        return CellIdentifiers.identifierFor(type(of: self))
    }
    
    internal func processLayout(_ width: CGFloat, collectionView: BaseCollectionViewController) {
        processLayout(width)
    }
    
    func processLayout(_ width: CGFloat) {
        
    }
    
    func reprocessLayout() {
        processLayout(cellLayout.cellWidth)
    }
}
