//
//  BaseAutolayoutCellViewModel.swift
//  SkinHack
//
//  Created by Игорь on 10/06/16.
//  Copyright © 2016 ballr. All rights reserved.
//

import Foundation

class BaseAutolayoutCellViewModel: BaseCellViewModel {
    
    static var autoLayoutCellsPrototypes: [String: CollectionBaseCell] = [:]
    
    internal func process(width: CGFloat) -> CGFloat {
        return width
    }
    
    override func processLayout(_ width: CGFloat, collectionView: BaseCollectionViewController) {
        let width = process(width: width)
        let identifier = CellIdentifiers.identifierFor(type(of: self))
        var prototype: CollectionBaseCell!
        prototype = BaseAutolayoutCellViewModel.autoLayoutCellsPrototypes[identifier]
        if prototype == nil {
            let cellType = collectionView.classForIdentifier(identifier)
            prototype = cellType.init()
            BaseAutolayoutCellViewModel.autoLayoutCellsPrototypes[identifier] = prototype
            prototype.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: 0))
        }
        prototype.setNeedsLayout()
        prototype.layoutIfNeeded()
        prototype.configurate(self, visible: false, prototype: true)
        prototype.setNeedsLayout()
        prototype.layoutIfNeeded()
        let height = prototype.contentView.systemLayoutSizeFitting(CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)).height
        self.cellLayout = AutolayoutCellLayout(width: width, cellSize: CGSize(width: width, height: height))
    }
    
    var canProcessInBackground: Bool {
        return cellHeight != nil
    }
    
    func processInBackground(_ width: CGFloat) {
        if let height = cellHeight {
            let width = process(width: width)
            self.cellLayout = AutolayoutCellLayout(width: width, cellSize: CGSize(width: width, height: height))
        }
    }
    
    var cellHeight: CGFloat? {
        return nil
    }
}
