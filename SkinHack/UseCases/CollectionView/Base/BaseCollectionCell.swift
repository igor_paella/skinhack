//
//  BaseCollectionCell.swift
//  SkinHack
//
//  Created by Игорь on 13/01/16.
//  Copyright © 2016 ballr. All rights reserved.
//

import Foundation
import UIKit
import Cartography

protocol CellHeightDelegate: class {
    func cellHeightDidChange(_ vmId: String)
}

class CollectionBaseCell: UICollectionViewCell {
    
    internal weak var heightDelegate: CellHeightDelegate?
    
    internal func configurate(_ item: BaseCellViewModel, visible: Bool, prototype: Bool) {
        
    }
    
    internal func willDisplay() {
    }
    
    internal func didEndDisplay() {
    }
}

class BaseCollectionCell<TViewModel: BaseCellViewModel, TLayout: BaseCellLayout>: CollectionBaseCell {

    internal weak var item: TViewModel?
    internal var separator: UIView!
    
    internal var selectable: Bool {
        return false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        contentView.frame = bounds
        super.layoutSubviews()
        
    }

    fileprivate func setup() {
        
        if selectable {
            let selectedView = UIView(frame: bounds)
            selectedView.backgroundColor = Colors.black.withAlphaComponent(0.3)
            selectedBackgroundView = selectedView
        }
        
        setupViews()
    }
    
    internal func setupViews() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        constrain(contentView) { contentView in
            
            contentView.top == contentView.superview!.top
            contentView.bottom == contentView.superview!.bottom ~ LayoutPriority(rawValue: 999)
            contentView.left == contentView.superview!.left
            contentView.right == contentView.superview!.right
        }
    }
    
    internal func addSeparator(_ color: UIColor, inset: UIEdgeInsets, height: CGFloat = 0.5) {
        separator = UIView()
        separator!.backgroundColor = color
        contentView.addSubview(separator!)
        
        constrain(separator!) { separator in
            separator.height == height
            separator.bottom == separator.superview!.bottom - inset.bottom
            separator.left == separator.superview!.left + inset.left
            separator.right == separator.superview!.right - inset.right
        }
    }
    
    internal func raiseHeightChanged() {
        if let itemId = item?.id {
            heightDelegate?.cellHeightDidChange(itemId)
        }
    }
    
    // method for autolayout cells (with prototypes)
    internal func configurate(_ item: TViewModel, visible: Bool, prototype: Bool) {
        configurate(item, visible: visible)
    }
    
    // method for frame layout cells
    internal func configurate(_ item: TViewModel, visible: Bool) {
        self.item = item
        contentView.backgroundColor = item.background ?? UIColor.clear
        if let cellLayout = item.cellLayout as? TLayout {
            applyLayout(cellLayout, visible: visible)
        }
    }
    
    override func configurate(_ item: BaseCellViewModel, visible: Bool, prototype: Bool) {
        if let item = item as? TViewModel {
            self.configurate(item, visible: visible, prototype: prototype)
        }
    }
    
    internal func applyLayout(_ layout: TLayout, visible: Bool) {
        
    }
}
