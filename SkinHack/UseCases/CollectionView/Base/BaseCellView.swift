//
//  BaseCellView.swift
//  SkinHack
//
//  Created by Игорь on 19/04/16.
//  Copyright © 2016 ballr. All rights reserved.
//

import Foundation

class BaseCellView: UIView {
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func setupViews() {
        
    }
}
