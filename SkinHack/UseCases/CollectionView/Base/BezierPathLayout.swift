//
//  BezierPathLayout.swift
//  SkinHack
//
//  Created by Игорь on 16/04/16.
//  Copyright © 2016 ballr. All rights reserved.
//

import Foundation

class BezierPathLayout: ViewLayout {
    
    var path: UIBezierPath?
    var strokeColor: UIColor
    var fillColor: UIColor
    var lineDashPattern: [NSNumber]?
    var borderWidth: CGFloat = 0
    
    init(frame: CGRect, path: UIBezierPath?, strokeColor: UIColor, fillColor: UIColor, borderWidth: CGFloat, lineDashPattern: [NSNumber]?, hidden: Bool = false) {
        self.path = path
        self.strokeColor = strokeColor
        self.fillColor = fillColor
        self.borderWidth = borderWidth
        self.lineDashPattern = lineDashPattern
        
        super.init(frame: frame, hidden: hidden)
    }
}
