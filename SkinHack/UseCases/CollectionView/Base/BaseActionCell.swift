//
//  BaseActionCell.swift
//  SkinHack
//
//  Created by Игорь on 02/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class ActionButton {
    var index: Int
    var title: String
    var color: UIColor
    var width: CGFloat
    
    init(index: Int, title: String, color: UIColor, width: CGFloat = 105) {
        self.index = index
        self.title = title
        self.color = color
        self.width = width
    }
}

class BaseActionCell<TViewModel: BaseCellViewModel, TLayout: BaseCellLayout>: BaseCollectionCell<TViewModel, TLayout> {
    
    var actions: [ActionButton] {
        return []
    }
    private var actionButtons: [UIButton] = []
    internal var container: UIView!
    private var containerLeft: NSLayoutConstraint!
    private var buttonsWidth: CGFloat = 0
    private var actionsShowed: Bool = false
    
    override func setupViews() {
        super.setupViews()
        
        var prev: UIView = self
        for (index, action) in actions.reversed().enumerated() {
            let button = UIButton()
            button.backgroundColor = action.color
            button.tag = action.index
            button.setTitle(action.title, for: .normal)
            button.setTitleColor(Colors.white, for: .normal)
            button.addTarget(self, action: #selector(BaseActionCell.touch(button:)), for: .touchUpInside)
            contentView.addSubview(button)
            buttonsWidth += action.width
            
            constrain(button, prev) { button, prev in
                button.top == button.superview!.top
                button.bottom == button.superview!.bottom
                button.width == action.width
                if index == 0 {
                    button.right == prev.right
                } else {
                    button.right == prev.left
                }
            }
            prev = button
        }
        
        container = UIView()
        container.backgroundColor = UIColor.white
        contentView.addSubview(container)
        
        container.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(BaseActionCell.pan(recognizer:))))
        container.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BaseActionCell.tap)))
        constrain(container) { container in
            container.size == container.superview!.size
            container.top == container.superview!.top
            containerLeft = container.left == container.superview!.left
        }
    }
    
    @objc private func touch(button: UIButton) {
        tap()
        actionTouched(index: button.tag)
    }
    
    func actionTouched(index: Int) {
        
    }
    
    @objc private func tap() {
        if actionsShowed {
            actionsShowed = false
            animateContainer()
        }
    }
    
    private var startX: CGFloat = 0
    @objc private func pan(recognizer: UIPanGestureRecognizer) {
        let location = recognizer.location(in: self)
        switch recognizer.state {
        case .began:
            startX = location.x - containerLeft.constant
        case .changed:
            let diffX = location.x - startX
            let newX = min(0, diffX)
            containerLeft.constant = newX
        case .ended, .cancelled, .failed:
            let velocity = recognizer.velocity(in: self)
            actionsShowed = velocity.x < 0
            animateContainer()
        default:
            break
        }
    }
    
    private func animateContainer() {
        containerLeft.constant = actionsShowed ? -buttonsWidth : 0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.superview?.layoutIfNeeded()
        }, completion: { _ in
            
        })
    }
    
    internal func shakeAnimate() {
        containerLeft.constant = -buttonsWidth
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.superview?.layoutIfNeeded()
        }, completion: { _ in
            self.containerLeft.constant = 0
            UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 5, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.superview?.layoutIfNeeded()
            }, completion: nil)
        })
    }
}
