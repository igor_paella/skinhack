//
//  AttributedTextLayout.swift
//  SkinHack
//
//  Created by Игорь on 10/01/16.
//  Copyright © 2016 ballr. All rights reserved.
//

// swiftlint:disable force_cast

import Foundation

class AttributedTextLayout: ViewLayout {
    
    var text: NSAttributedString!
    var aligment: NSTextAlignment!
    
    init(frame: CGRect, text: NSAttributedString, aligment: NSTextAlignment = .left, hidden: Bool = false) {
        self.text = text
        self.aligment = aligment
        
        super.init(frame: frame, hidden: hidden)
    }
    
    init(text: NSAttributedString, width: CGFloat, origin: CGPoint, aligment: NSTextAlignment = .left) {
        self.text = text
        self.aligment = aligment
        
        let textHeight = text.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).height
        super.init(frame: CGRect(x: origin.x, y: origin.y, width: width, height: textHeight))
    }
    
    override class var emptyLayout: AttributedTextLayout {
        return AttributedTextLayout(frame: CGRect.zero, text: NSAttributedString(string: ""), hidden: true)
    }
}

extension UILabel {
    func applyLayout(_ layout: AttributedTextLayout) {
        isHidden = layout.hidden
        if isHidden { return }
        frame = layout.frame
        attributedText = layout.text
        textAlignment = layout.aligment
    }
}

extension UITextView {
    func applyLayout(_ layout: AttributedTextLayout) {
        isHidden = layout.hidden
        if isHidden { return }
        frame = layout.frame
        attributedText = layout.text
        textAlignment = layout.aligment
    }
}

extension UIButton {
    func applyLayout(_ layout: AttributedTextLayout) {
        isHidden = layout.hidden
        if isHidden { return }
        frame = layout.frame
        setAttributedTitle(layout.text, for: .normal)
        let invText = layout.text.mutableCopy() as! NSMutableAttributedString
        invText.addAttribute(NSAttributedStringKey.foregroundColor, value: Colors.black, range: NSRange(location: 0, length: invText.length))
        setAttributedTitle(invText, for: .highlighted)
    }
}
