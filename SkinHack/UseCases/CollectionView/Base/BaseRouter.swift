//
//  Router.swift
//  SkinHack
//
//  Created by Игорь on 02/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class Router {
    class RouterSegue {
        var vc: ViewControllerBase
        weak var context: UIViewController?
        
        init(vc: ViewControllerBase, context: UIViewController?) {
            self.vc = vc
            self.context = context
        }
    }
    
    static var top: Router {
        let router = Router()
        router.context = UIApplication.topVC
        return router
    }
    
    static var main: UIViewController?
    weak var context: UIViewController? // only for Routing purpose
}
