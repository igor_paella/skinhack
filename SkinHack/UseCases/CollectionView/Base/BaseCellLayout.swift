//
//  BaseCellLayout.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class BaseCellLayout: ViewLayout {
    var cellWidth: CGFloat = 0
    
    init(width: CGFloat, frame: CGRect, hidden: Bool = false) {
        super.init(frame: frame, hidden: hidden)
        
        self.cellWidth = width
    }
    
    init(width: CGFloat, cellSize: CGSize, hidden: Bool = false) {
        super.init(frame: CGRect(origin: CGPoint.zero, size: cellSize), hidden: hidden)
        
        self.cellWidth = width
    }
}
