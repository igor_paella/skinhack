//
//  CalendarCellViewModel.swift
//  SkinHack
//
//  Created by Ivan Starchenkov on 11/25/17.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class CalendarCellViewModel: BaseAutolayoutCellViewModel {
    
    var calories: Float = 0.0
    var date: String!
    
    init(date: String? = nil, calories: Float = 0) {
        var identifier = "space"
        if let date = date {
            identifier = date
        }
        super.init(id: identifier)
        self.calories = calories
        self.date = identifier
    }
    
    override var cellHeight: CGFloat? {
        return screenWidth / 7.0
    }
    
    override func process(width: CGFloat) -> CGFloat {
        return width / 7.0
    }
    
    func today() -> Bool {
        return Cal.stringRepresentToday(dateString: self.date)
    }
    
    func space() -> Bool {
        return self.id == "space"
    }
}
