//
//  CalendarSectionView.swift
//  SkinHack
//
//  Created by Ivan Starchenkov on 11/25/17.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class CalendarSectionView: UICollectionReusableView {
    
    private var title: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = Colors.clear
        
        title = UILabel()
        title.font = Fonts.Display.bold(20)
        title.textColor = Colors.white
        addSubview(title)
        
        constrain(title) { title in
            title.centerX == title.superview!.centerX
            title.centerY == title.superview!.centerY + 5
        }
    }
    
    func configurate(title: String) {
        self.title.text = title
    }
}
