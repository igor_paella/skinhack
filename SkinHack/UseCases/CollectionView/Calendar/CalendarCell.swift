//
//  CalendarCell.swift
//  SkinHack
//
//  Created by Ivan Starchenkov on 11/25/17.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class CalendarCell: BaseCollectionCell<CalendarCellViewModel, AutolayoutCellLayout> {
    
    var image: UIImageView!
    var background: UIView!
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = Colors.clear
        
        background = UIView()
        contentView.addSubview(background)
        
        image = UIImageView()
        image.contentMode = .scaleAspectFit
        contentView.addSubview(image)
        
        constrain(background, image) { background, image in
            background.left == background.superview!.left + 0.5
            background.right == background.superview!.right - 0.5
            background.top == background.superview!.top + 0.5
            background.bottom == background.superview!.bottom - 0.5

            image.centerX == image.superview!.centerX
            image.centerY == image.superview!.centerY
            image.width == 32
            image.height == 32
        }
    }
    
    override func configurate(_ item: CalendarCellViewModel, visible: Bool, prototype: Bool) {
        super.configurate(item, visible: visible, prototype: prototype)
        background.backgroundColor = item.space() ? Colors.clear : (item.today() ? Colors.calendarCellToday : Colors.calendarCell)
        image.image = (item.space() || item.calories == 0) ? nil : UIImage(named: Product.init(calories: item.calories)!.rawValue)
    }
}
