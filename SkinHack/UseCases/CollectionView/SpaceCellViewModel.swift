//
//  SpaceCellViewModel.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class SpaceCellViewModel: BaseAutolayoutCellViewModel {
    
    var height: CGFloat = 0
    
    init(height: CGFloat) {
        super.init()
        
        self.height = height
    }
    
    override var cellHeight: CGFloat? {
        return height
    }
}
