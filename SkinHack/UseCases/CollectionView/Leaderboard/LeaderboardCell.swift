//
//  LeaderboardCell.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class LeaderboardCell: BaseCollectionCell<LeaderboardCellViewModel, AutolayoutCellLayout> {
    
    var titleView: UILabel!
    var avatarView: UIImageView!
    var resImage: UIImageView!
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = Colors.clear
        
        avatarView = UIImageView()
        avatarView.contentMode = .scaleAspectFill
        avatarView.layer.cornerRadius = 20
        avatarView.layer.masksToBounds = true
        avatarView.backgroundColor = Colors.gray
        contentView.addSubview(avatarView)
        
        titleView = UILabel()
        titleView.font = Fonts.Display.medium(20)
        titleView.textColor = Colors.white.withAlphaComponent(0.54)
        contentView.addSubview(titleView)
        
        resImage = UIImageView()
        resImage.contentMode = .scaleAspectFit
        contentView.addSubview(resImage)
        
        constrain(avatarView, titleView, resImage) { avatarView, titleView, resImage in
            
            avatarView.width == 40
            avatarView.height == 40
            avatarView.centerY == avatarView.superview!.centerY
            avatarView.left == avatarView.superview!.left + 20
            
            titleView.centerY == titleView.superview!.centerY
            titleView.left == avatarView.right + 10
            titleView.right == titleView.superview!.right - 65
            
            resImage.width == 32
            resImage.height == 32
            resImage.centerY == resImage.superview!.centerY
            resImage.centerX == resImage.superview!.right - 38
        }
    }
    
    override func configurate(_ item: LeaderboardCellViewModel, visible: Bool, prototype: Bool) {
        super.configurate(item, visible: visible, prototype: prototype)
        
        contentView.backgroundColor = item.selected ? UIColor(netHex: 0x2DEFFF).withAlphaComponent(0.3) : Colors.gray.withAlphaComponent(0.1)
        titleView.text = "\(item.order). \(item.item.name ?? "")"
        avatarView.setImageByUrl(item.item.avatar)
        resImage.image = item.item.res?.image
    }
}
