//
//  LeaderboardCellViewModel.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class LeaderboardCellViewModel: BaseAutolayoutCellViewModel {
    static var height: CGFloat = 60
    
    var order: Int
    var item: LeaderboardItem
    var selected: Bool
    
    init(item: LeaderboardItem, order: Int, selected: Bool) {
        self.item = item
        self.order = order
        self.selected = selected
        super.init(id: "LeaderboardCellViewModel_\(item.id)")
    }
    
    override var cellHeight: CGFloat? {
        return LeaderboardCellViewModel.height
    }
}
