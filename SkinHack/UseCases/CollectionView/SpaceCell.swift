//
//  SpaceCell.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class SpaceCell: BaseCollectionCell<SpaceCellViewModel, AutolayoutCellLayout> {
    
    private var height: NSLayoutConstraint!
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = Colors.clear
        
        constrain(contentView) { content in
            height = (content.height == 0)
        }
    }
    
    override func configurate(_ item: SpaceCellViewModel, visible: Bool, prototype: Bool) {
        super.configurate(item, visible: visible, prototype: prototype)
        
        height.constant = item.height
    }
}
