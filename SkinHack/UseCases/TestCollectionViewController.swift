//
//  TestCollectionViewController.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class TestCollectionViewController: BaseCollectionViewController, ViewModelHolder {
    
    typealias TViewModel = TestCollectionViewModelProtocol

    var heroView: UILabel!
    
    override var cellType: CellsRegistrotor {
        return .test(self)
    }
    
    override func setupView() {
        super.setupView()
        
        heroView = UILabel()
        heroView.heroID = "heroTest"
        heroView.backgroundColor = UIColor.blue
        heroView.text = "Hero test"
        heroView.textAlignment = .center
        view.addSubview(heroView)
        
        collectionView.heroModifiers = [.translate(y:100)]
        
        constrain(heroView) { heroView in
            heroView.centerX == heroView.superview!.centerX
            heroView.bottom == heroView.superview!.bottom - 50
            heroView.width == 200
            heroView.height == 50
        }
        
        viewModel.didLoad()
    }
}
