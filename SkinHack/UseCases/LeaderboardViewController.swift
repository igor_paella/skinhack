//
//  LeaderboardViewController.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class LeaderboardViewController: BaseCollectionViewController, ViewModelHolder {
    
    typealias TViewModel = LeaderboardViewModelProtocol
    
    var titleView: UILabel!
    var indicator: UIActivityIndicatorView!
    var homeButton: UIButton!
    
    override var cellType: CellsRegistrotor {
        return .lead(self)
    }
    
    override func setupView() {
        super.setupView()
        
        view.backgroundColor = Colors.background
        
        titleView = UILabel()
        titleView.textColor = Colors.white
        titleView.text = "Leaderboard"
        titleView.font = Fonts.Display.bold(24)
        collectionView.addSubview(titleView)
        
        indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        indicator.hidesWhenStopped = true
        view.addSubview(indicator)
        
        homeButton = UIButton()
        homeButton.titleLabel?.font = Fonts.Display.black(36)
        homeButton.setTitle("HOME", for: .normal)
        homeButton.setTitleColor(Colors.cyan, for: .normal)
        homeButton.backgroundColor = Colors.background.withAlphaComponent(0.9)
        homeButton.addTarget(self, action: #selector(LeaderboardViewController.homeTouched), for: .touchUpInside)
        view.addSubview(homeButton)
        
        constrain(titleView, indicator, homeButton) { titleView, indicator, homeButton in
            titleView.bottom == titleView.superview!.bottom - 16
            titleView.centerX == titleView.superview!.centerX
            
            indicator.centerX == indicator.superview!.centerX
            indicator.centerY == indicator.superview!.centerY
            
            homeButton.left == homeButton.superview!.left
            homeButton.right == homeButton.superview!.right
            homeButton.height == 80
            homeButton.bottom == homeButton.superview!.bottom - 52
        }
        
        collectionView.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 132, right: 0)
        collectionView.scrollIndicatorInsets = collectionView.contentInset
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator.startAnimating()
    }
    
    override func updateItemsCompleted() {
        indicator.stopAnimating()
    }
    
    @objc private func homeTouched() {
        if viewModel.isModal {
            viewModel.router.close()
        } else {
            viewModel.router.start().presentNav()
        }
    }
}
