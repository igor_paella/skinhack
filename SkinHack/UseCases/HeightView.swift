//
//  HeightView.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation
import Cartography

class HeightView: BaseView {
    var title: UILabel!
    var selectView: SelectView!
    var nextButton: UIButton!
    
    override func setupView() {
        super.setupView()
        
        backgroundColor = Colors.background
        
        title = UILabel()
        title.textColor = Colors.white
        title.text = "Your height"
        title.font = Fonts.Display.bold(24)
        addSubview(title)
        
        selectView = SelectView()
        selectView.valueDesc = "CM"
        addSubview(selectView)
        
        nextButton = UIButton()
        nextButton.setTitle("NEXT", for: .normal)
        nextButton.titleLabel?.font = Fonts.Display.black(36)
        nextButton.setTitleColor(Colors.white, for: .normal)
        addSubview(nextButton)
        
        constrain(title, selectView, nextButton) { title, selectView, nextButton in
            title.top == title.superview!.top + 35
            title.centerX == title.superview!.centerX
            
            selectView.left == selectView.superview!.left
            selectView.right == selectView.superview!.right
            selectView.centerY == selectView.superview!.centerY
            
            nextButton.centerX == nextButton.superview!.centerX
            nextButton.bottom == nextButton.superview!.bottom - 70
        }
    }
}
