//
//  DeviceListViewController.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//
// swiftlint:disable force_cast

import Foundation
import Cartography

class CalendarViewController: BaseCollectionViewController, ViewModelHolder {
    typealias TViewModel = CalendarViewModelProtocol
    
    var homeButton: UIButton!
    private var firstLoaded = false
    
    override var cellType: CellsRegistrotor {
        return .calendar(self)
    }
    
    override func setupView() {
        super.setupView()
        view.backgroundColor = Colors.gray
        
        homeButton = UIButton()
        homeButton.titleLabel?.font = Fonts.Display.black(36)
        homeButton.setTitle("HOME", for: .normal)
        homeButton.setTitleColor(Colors.cyan, for: .normal)
        homeButton.backgroundColor = Colors.background.withAlphaComponent(0.9)
        homeButton.addTarget(self, action: #selector(CalendarViewController.homeTouched), for: .touchUpInside)
        view.addSubview(homeButton)
        
        constrain(homeButton) { homeButton in
            homeButton.left == homeButton.superview!.left
            homeButton.right == homeButton.superview!.right
            homeButton.height == 80
            homeButton.bottom == homeButton.superview!.bottom - 52
        }
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 132, right: 0)
        collectionView.scrollIndicatorInsets = collectionView.contentInset
        collectionView.register(CalendarSectionView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Section")
    }
    
    override func updateItemsCompleted() {
        if !firstLoaded {
            firstLoaded = true
            if let attributes = collectionView.collectionViewLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: IndexPath(item: 0, section: Cal.todaysMonth())) {
                collectionView.setContentOffset(CGPoint(x: 0, y: attributes.frame.origin.y + attributes.frame.height - screenHeight ), animated: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Section", for: indexPath) as! CalendarSectionView
        header.configurate(title: viewModel.titleForSectionAt(index: indexPath.section))
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 0, height: 45)
    }
    
    @objc private func homeTouched() {
        viewModel.router.close()
    }
}
