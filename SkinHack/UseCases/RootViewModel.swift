//
//  RootViewModel.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol RootViewModelProtocol: BaseViewModelProtocol {    
}

class RootViewModel: BaseViewModel, RootViewModelProtocol {
    var authFacade: AuthFacadeProtocol!
    
    override func didAppear() {
        super.didAppear()

//        authFacade.logout()

        if authFacade.isAuthorized {
            router.deviceList().presentNav(animated: false, constructNav: { NavigationController(rootViewController: $0) })
        } else {
            router.auth { r in
                r.weight { r in
                    r.height { r in
                        r.deviceList().presentNav(constructNav: { NavigationController(rootViewController: $0) })
                    }.push()
                }.push()
            }.presentNav(animated: false, constructNav: { NavigationController(rootViewController: $0) })
        }
    }
}
