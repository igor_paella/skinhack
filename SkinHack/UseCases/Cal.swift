//
//  Cal.swift
//  SkinHack
//
//  Created by Ivan Starchenkov on 11/25/17.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class Cal {
    
    class func getAllDaysOf(month: Int) -> (Int, Int) {
        let dateComponents = DateComponents(year: 2017, month: month)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        let firstDayOfTheMonth = calendar.date(from: DateComponents(year: 2017, month: month, day: 1))!
        let offset = Calendar.current.component(.weekday, from: firstDayOfTheMonth)
        
        return (numDays, offset)
    }
    
    class func stringRepresentToday(dateString: String) -> Bool {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let todayString = dateFormatter.string(from: date)
        return todayString == dateString
    }
    
    class func todaysMonth() -> Int {
        return Calendar.current.component(.month, from: Date())
    }
}
