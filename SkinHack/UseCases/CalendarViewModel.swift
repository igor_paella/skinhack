//
//  DeviceListViewModel.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

protocol CalendarViewModelProtocol: CollectionViewModelProtocol {
    func titleForSectionAt(index: Int) -> String
}

class CalendarViewModel: BaseCollectionViewModel, CalendarViewModelProtocol {
    var tshirtFacade: TShirtFacadeProtocol!
    var userFacade: UserFacadeProtocol!
    
    var section: [String] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    
    private var workouts: [LeaderboardItem] = []
    
    override func willAppear() {
        loadItems()
        update()
    }
    
    func titleForSectionAt(index: Int) -> String {
        return section[index]
    }
    
    private func loadItems() {
        userFacade.getMyWorkouts { [weak self] items in
            self?.workouts = items
            self?.update()
        }
    }
    
    override func update() {
        var newItems: [[BaseCellViewModel]] = []
        for month in 1...12 {
            var monthItems: [BaseCellViewModel] = []
            let monthSetting = Cal.getAllDaysOf(month: month)
            if monthSetting.1 > 1 {
                for _ in 1...(monthSetting.1 - 1) {
                    let item = CalendarCellViewModel(date: nil, calories: 0)
                    monthItems.append(item)
                }
            }
            
            for day in 1...monthSetting.0 {
                let mm = month < 10 ? "0\(month)" : "\(month)"
                let dd = day < 10 ? "0\(day)" : "\(day)"
                let mmdd = "\(mm)-\(dd)"
                let workout = workouts.first(where: { $0.mmdd == mmdd })
                let dateText = "2017-\(mm)-\(dd)"
                let item = CalendarCellViewModel(date: dateText, calories: Float(workout?.result ?? 0))
                monthItems.append(item)
            }
            newItems.append(monthItems)
        }
        updated?(newItems, .firstLoad)
    }
    
    override func touched(item: BaseCellViewModel, indexPath: IndexPath) {
    }
}
