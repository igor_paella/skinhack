//
//  AuthViewController.swift
//  SkinHack
//
//  Created by Игорь on 25/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

class AuthViewController: BaseViewController<AuthView>, ViewModelHolder {
    typealias TViewModel = AuthViewModelProtocol
    
    override func setupView() {
        super.setupView()
        
        baseView.loginButton.addTarget(self, action: #selector(AuthViewController.loginTouched), for: .touchUpInside)
        
        viewModel.busyChanged = { Activity.instance.busy($0, yOffset: 150) }
    }

    @objc private func loginTouched() {
        viewModel.auth()
    }
}
