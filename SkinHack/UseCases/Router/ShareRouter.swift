//
//  ShareRouter.swift
//  SkinHack
//
//  Created by Игорь on 02/11/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

import Foundation

extension Router {
    public func phoneCall(number: String) {
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    public func message(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        context?.present(alert, animated: true, completion: nil)
    }
    
    public func alert(message: String, okay: (() -> Void)?, cancel: (() -> Void)?) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yup", style: .default, handler: { _ in okay?() }))
        alert.addAction(UIAlertAction(title: "Nope", style: .cancel, handler: { _ in cancel?() }))
        context?.present(alert, animated: true, completion: nil)
    }
    
    public func share(text: String) {
        let objectsToShare = [text]
        let vc = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        vc.excludedActivityTypes = [
            .airDrop,
            .addToReadingList,
            .copyToPasteboard,
            .assignToContact,
            .print,
            .openInIBooks,
            .postToFacebook,
            .postToFlickr,
            .postToTencentWeibo,
            .postToTwitter,
            .postToVimeo,
            .postToWeibo,
            .saveToCameraRoll,
            UIActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
            UIActivityType(rawValue: "com.apple.mobilenotes.SharingExtension"),
            UIActivityType(rawValue: "com.apple.CloudDocsUI.AddToiCloudDrive")
        ]
        context?.present(vc, animated: true, completion: nil)
    }
    
    public var appStoreLink: String {
        return "https://itunes.apple.com/app/id_"
    }
    
    public func openAppstore() {
        if let url = URL(string: appStoreLink) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
