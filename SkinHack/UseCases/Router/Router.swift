//
//  Router.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//
// swiftlint:disable force_try

import Foundation

extension Router {
    
    typealias Segue = Router.RouterSegue
    
    static func rootInit(vc: RootViewController) {
        let vm = try! container.resolve() as RootViewModelProtocol
        vm.router.context = vc
        vc.vm = vm
    }
    
    func leaderboard(isModal: Bool) -> Segue {
        let vm = try! container.resolve(arguments: isModal) as LeaderboardViewModelProtocol
        return construct(vm: vm, LeaderboardViewController.self)
    }
    
    func height(done: ((Router) -> Void)?) -> Segue {
        let vm = try! container.resolve(arguments: done) as HeightViewModelProtocol
        return construct(vm: vm, HeightViewController.self)
    }
    
    func weight(done: ((Router) -> Void)?) -> Segue {
        let vm = try! container.resolve(arguments: done) as WeightViewModelProtocol
        return construct(vm: vm, WeightViewController.self)
    }
    
    func auth(done: ((Router) -> Void)?) -> Segue {
        let vm = try! container.resolve(arguments: done) as AuthViewModelProtocol
        return construct(vm: vm, AuthViewController.self)
    }
    
    func test() -> Segue {
        let vm = try! container.resolve() as TestViewModelProtocol
        return construct(vm: vm, TestViewController.self)
    }
    
    func collectionTest(string: String) -> Segue {
        let vm = try! container.resolve(arguments: string) as TestCollectionViewModelProtocol
        return construct(vm: vm, TestCollectionViewController.self)
    }
    
    func start() -> Segue {
        let vm = try! container.resolve() as StartViewModelProtocol
        return construct(vm: vm, StartViewController.self)
    }
    
    func deviceList() -> Segue {
        let vm = try! container.resolve() as DeviceListViewModelProtocol
        return construct(vm: vm, DeviceListViewController.self)
    }
    
    func calendar() -> Segue {
        let vm = try! container.resolve() as CalendarViewModelProtocol
        return construct(vm: vm, CalendarViewController.self)
    }

}
