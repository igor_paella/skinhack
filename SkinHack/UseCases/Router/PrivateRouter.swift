//
//  PrivateRouter.swift
//  SkinHack
//
//  Created by Игорь on 17/03/2017.
//  Copyright © 2017 xuli. All rights reserved.
//

// swiftlint:disable force_cast

import Foundation

extension Router {
    
    internal func construct<T, TVC: ViewControllerBase>(vm: T, _: TVC.Type) -> Router.RouterSegue {
        let vc = TVC()
        vc.vm = vm as! BaseViewModelProtocol
        (vm as! BaseViewModelProtocol).router.context = vc
        return Router.RouterSegue(vc: vc, context: context)
    }
    
    public func pop() {
        _ = context?.navigationController?.popViewController(animated: true)
    }
    
    public func  popToRoot() {
        _ = context?.navigationController?.popToRootViewController(animated: true)
    }
    
    public func close(completion: (() -> Void)? = nil) {
        context?.dismiss(animated: true, completion: completion)
    }
    
    public func removeFromNavStack() {
        guard let context = context else { return }
        context.navigationController?.viewControllers.remove(object: context)
    }
    
    public var isVisible: Bool {
        if context?.view?.window != nil {
            return true
        }
        return false
    }
}

extension Router.RouterSegue {
    
    internal func pushPresent(animated: Bool = true) {
        pushPresent(animated: animated) { vc in
            return UINavigationController(rootViewController: vc)
        }
    }
    
    internal func pushPresent(animated: Bool = true, constructNav: (UIViewController) -> (UINavigationController)) {
        if context?.navigationController != nil {
            push(animated: animated)
        } else {
            presentNav(animated: animated, constructNav: constructNav)
        }
    }
    
    internal func push(animated: Bool = true) {
        context?.navigationController?.pushViewController(vc, animated: animated)
    }
    
    internal func presentNavHero() {
        presentNav(constructNav: { vc in
            return HeroNavigationController(rootViewController: vc)
        })
    }
    
    internal func presentNav(animated: Bool = true, style: UIModalPresentationStyle? = nil, constructNav: (UIViewController) -> (UINavigationController), completion: (() -> Void)? = nil) {
        
        let nav = constructNav(vc)
        nav.navigationBar.isHidden = true
        if let style = style {
            nav.modalPresentationStyle = style
        }
        vc.transitioningDelegate = context as? UIViewControllerTransitioningDelegate
        context?.present(nav, animated: animated, completion: completion)
    }
    
    internal func presentNav(animated: Bool = true, style: UIModalPresentationStyle? = nil, completion: (() -> Void)? = nil) {
        presentNav(animated: animated, style: style, constructNav: { vc in
            return UINavigationController(rootViewController: vc)
        }, completion: completion)
    }
    
    internal func present(animated: Bool = true, completion: (() -> Void)? = nil) {
        vc.transitioningDelegate = context as? UIViewControllerTransitioningDelegate
        context?.present(vc, animated: animated, completion: completion)
    }
    
    internal func replaceRoot(animated: Bool = true, completion: (() -> Void)? = nil) {
        (UIApplication.shared.delegate as? AppDelegate)?.window?.replaceRootVCWith(vc, animated: animated, completion: completion)
    }
    
    internal func replaceRootNav(animated: Bool = true, completion: (() -> Void)? = nil) {
        (UIApplication.shared.delegate as? AppDelegate)?.window?.replaceRootVCWith(UINavigationController(rootViewController: vc), animated: animated, completion: completion)
    }
    
    // MARK: - HERO
    
    internal func heroPresent() {
        context?.hero_replaceViewController(with: vc)
    }
    
    internal func heroDismiss() {
        context?.hero_dismissViewController()
    }
}
